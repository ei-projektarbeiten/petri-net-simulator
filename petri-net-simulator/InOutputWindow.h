#pragma once
#include <QTableWidget>
#include <QWidget>

#include "MainWindow.h"

class InOutputWindow :
    public QWidget
{

public:
    InOutputWindow(QFont*, const QList<QString>&, const QList<QString>&, MainWindow*);
    ~InOutputWindow() override;
    void updateOutputValues(const QMap<QString, bool>&);
    [[nodiscard]] QMap<QString, bool> getInputValues() const;

private:
    void loadInputsIntoTable(const QList<QString>&);
    void loadOutputsIntoTable(const QList<QString>&);

    QFont* font;
    QTableWidget* inputTable;
    QTableWidget* outputTable;
    MainWindow* mainWindow;
};

