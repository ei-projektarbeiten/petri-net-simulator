#pragma once

#include <QGraphicsView>
#include <QMouseEvent>

#include "ContentViewGraphicsScene.fwd.h"

#include "ContentViewCursor.h"
#include "AppState.h"
#include "InOutputWindow.h"

class ContentView final : public QGraphicsView
{
public:
    ~ContentView() override;
    ContentView(QWidget* parent, ContentViewCursor cursor, AppMode appState, QFont* globalFont, unsigned nextId = 0);
    void fontChanged();
    [[nodiscard]] QList<QString> getOutputNames() const;
    [[nodiscard]] QList<QString> getInputNames() const;
    [[nodiscard]] QMap<QString, bool> automaticRunTriggered(const QMap<QString, bool>&) const;
    void edit() const;
    void setCurrentCursor(ContentViewCursor newCursor);
    void setAppState(AppMode newState);
    [[nodiscard]] ContentViewCursor getContentViewCursor() const;
    [[nodiscard]] AppMode getAppMode() const;
    static void slotCustomMenuRequested(QPoint);
    void deleteSelectedItem() const;
    [[nodiscard]] ContentViewGraphicsScene* getContentViewGraphicsScene() const { return graphicsScene; }

private slots:
    void resizeEvent(QResizeEvent *event) override;

private:

    ContentViewCursor contentViewCursor;
    AppMode appMode;

    ContentViewGraphicsScene* graphicsScene;
    QFont* globalFont;
};

