#pragma once

#include <QGraphicsItem>
#include "ContentViewCursor.h"
#include "AppMode.h"

class AppState
{
public:
	// Add Arc
	static void componentSelectedForArc(QGraphicsItem*);
	void setAppMode(AppMode newAppMode);
	void setContentViewCursor(ContentViewCursor newContentViewCursor);
	static ContentViewCursor getContentViewCursor();
	static AppMode getAppMode();

private:
	ContentViewCursor contentViewCursor;
	AppMode appMode;

};