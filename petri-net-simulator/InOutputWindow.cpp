#include "InOutputWindow.h"

#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>

#include "MainWindow.h"
#include "Output.h"

InOutputWindow::InOutputWindow(QFont* font, const QList<QString>& inputs, const QList<QString>& outputs, MainWindow* mainWindow):QWidget(),font(font),mainWindow(mainWindow)
{
    resize(600, 400);

	setWindowTitle("Automatic Mode - Set Inputs - View Outputs");

    this->setAttribute(Qt::WA_QuitOnClose, true);        //
    this->setAttribute(Qt::WA_DeleteOnClose);               // Notwendig damit Destruktor aufgerufen wird

    const auto vbox = new QVBoxLayout(this);

    const auto description = new QLabel("Set the values for the inputs. Output Values will be computed after you have pressed the play button.", this);
    vbox->addWidget(description);

    const auto descriptionInputs = new QLabel("Inputs:", this);
    vbox->addWidget(descriptionInputs);

    inputTable = new QTableWidget(this);
    loadInputsIntoTable(inputs);

    vbox->addWidget(inputTable);

    const auto descriptionOutputs = new QLabel("Outputs:", this);
    vbox->addWidget(descriptionOutputs);

    outputTable = new QTableWidget(this);
    loadOutputsIntoTable(outputs);

    vbox->addWidget(outputTable);

    this->setLayout(vbox);

}

InOutputWindow::~InOutputWindow()
{
    mainWindow->inOutputWindowClosed();
    mainWindow = nullptr;
    // delete inputTable;
    // delete outputTable;
}

void InOutputWindow::updateOutputValues(const QMap<QString, bool>& newOutputValues)
{

    outputTable->clear();

    outputTable->setFont(*font);
    outputTable->setColumnCount(2);
    outputTable->setRowCount(0);

    outputTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Name"));
    outputTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Value"));

    auto keys = newOutputValues.keys();
    for (auto& key: keys)
    {
	    const bool value = newOutputValues.value(key);

        outputTable->insertRow(outputTable->rowCount());

	    const int rowIndex = outputTable->rowCount() - 1;

	    const auto lineViewName = new QLineEdit();
        lineViewName->setEnabled(false);
        lineViewName->setText(key);
        outputTable->setCellWidget(rowIndex, 0, lineViewName);

	    const auto combo = new QComboBox(this);
        combo->addItems({ "--", "false", "true" });
        if (value)
            combo->setCurrentIndex(2);
        else
            combo->setCurrentIndex(1);
        
        combo->setEnabled(false);
        outputTable->setCellWidget(rowIndex, 1, combo);

    }

    outputTable->setColumnWidth(0, 200);
}

QMap<QString, bool> InOutputWindow::getInputValues() const
{
    qInfo("getInputValues");

    QMap<QString, bool> inputMap;

    for (int i = 0; i < inputTable->rowCount(); i++)
    {
        const auto nameWidget = dynamic_cast<QLineEdit*>(inputTable->cellWidget(i, 0));
        const auto valueWidget = dynamic_cast<QComboBox*>(inputTable->cellWidget(i, 1));
        auto temp = valueWidget->currentText();
        if (valueWidget->currentText() == "true")
            inputMap.insert(nameWidget->text(), true);
        else
            inputMap.insert(nameWidget->text(), false);
    }
    
    return inputMap;
}

void InOutputWindow::loadInputsIntoTable(const QList<QString>& inputs)
{
    inputTable->setFont(*font);
    inputTable->setColumnCount(2);
    inputTable->setRowCount(0);

    inputTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Name"));
    inputTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Value"));

    for (int i = 0; i < inputs.count(); i++)
    {
        QString inputName = inputs.at(i);

        inputTable->insertRow(inputTable->rowCount());

        const int rowIndex = inputTable->rowCount() - 1;

        const auto lineViewName = new QLineEdit();
        lineViewName->setEnabled(false);
        lineViewName->setText(inputName);
        inputTable->setCellWidget(rowIndex, 0, lineViewName);

        const auto combo = new QComboBox(this);
        combo->addItems({ "false", "true" });
        combo->setCurrentIndex(0);
        combo->setEnabled(true);
        inputTable->setCellWidget(rowIndex, 1, combo);

    }

    inputTable->setColumnWidth(0, 200);
}

void InOutputWindow::loadOutputsIntoTable(const QList<QString>& outputs)
{
    outputTable->setFont(*font);
    outputTable->setColumnCount(2);
    outputTable->setRowCount(0);

    outputTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Name"));
    outputTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Value"));

    for (int i = 0; i < outputs.count(); i++)
    {
        QString outputName = outputs.at(i);

        outputTable->insertRow(outputTable->rowCount());

        const int rowIndex = outputTable->rowCount() - 1;

        const auto lineViewName = new QLineEdit();
        lineViewName->setEnabled(false);
        lineViewName->setText(outputName);
        outputTable->setCellWidget(rowIndex, 0, lineViewName);

        const auto combo = new QComboBox(this);
        combo->addItems({ "--", "false", "true"});
        combo->setCurrentIndex(0);
        combo->setEnabled(false);
        outputTable->setCellWidget(rowIndex, 1, combo);
        
    }

    outputTable->setColumnWidth(0, 200);
}
