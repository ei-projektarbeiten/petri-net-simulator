#include "Transition.h"

#include <QMenu>
#include <sstream>

#include "TransitionAdditionalConditionsDialog.h"

Transition::~Transition()
{
    this->contentViewGraphicsScene->removeTransitionFromList(this);
    this->contentViewGraphicsScene = nullptr;


    // delete qGraphicsTextItem;	// deleted by QT
}

Transition::Transition(ContentViewGraphicsScene* contentViewGraphicsScene, unsigned id, QFont* newGlobalFont) :
    QGraphicsRectItem(QRectF(0.0,0.0,20,20)),
    PetriNetMainComponent(id, PetriNetMainComponentType::Transition, newGlobalFont)
{    
    this->contentViewGraphicsScene = contentViewGraphicsScene;

    this->setFlag(ItemSendsScenePositionChanges, true);
    this->setFlag(ItemIsSelectable, true);
    this->setFlag(ItemIsMovable, true);

    //rectItem->setBrush(QBrush(Qt::red, Qt::SolidPattern));

    itemPen = new QPen(Qt::black);
    itemPen->setWidth(3);
    this->setPen(*itemPen);

    // Bring to foreground
    this->setZValue(1);

    this->qGraphicsTextItem = nullptr;
    this->name = nullptr;

    automaticRunIsFireable = false;
}

Transition::Transition(ContentViewGraphicsScene* contentViewGraphics, QFont* font, unsigned id, const QString& newName,
	const QString& additionalConditions): Transition(contentViewGraphics, id, font)
{

    if (!newName.isNull()&&!newName.isEmpty())
		this->name = newName;
    if (!additionalConditions.isNull() && !additionalConditions.isEmpty())
		this->additionalConditions = additionalConditions;

    updateTextView();
}

void Transition::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    qInfo("Transition Clicked ");

    if (event->button() == Qt::LeftButton && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::Arc)
    {
        contentViewGraphicsScene->addArcToComponent(this);
    }
    else if (event->button() == Qt::LeftButton 
        && this->contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::FireTransition
        && couldTransitionFire(false))
    {
        fireTransition();
        this->contentViewGraphicsScene->updateTransitionColor();
	}
    else if (event->button() == Qt::RightButton 
        && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::Select
        && this->contains(event->pos()))
    {
        // open Menu
        openContextMenu(event->scenePos());
    }
    else {
        QGraphicsRectItem::mousePressEvent(event);
    }
}

void Transition::openContextMenu(QPointF pos)
{
	const auto contentView = this->contentViewGraphicsScene->getContentView();

    /* Create an object context menu */
	const auto menu = new QMenu(contentView);
    menu->setFont(*globalFont);

    /* Create actions to the context menu */
    const auto setAdditionalTransitionConditions = new QAction("set additional transition conditions", this);
	const auto setName = new QAction("set name", this);
	const auto deleteItem = new QAction("delete", this);
    deleteItem->setShortcut(Qt::Key_Delete);

    // disable when not AppMode::Edit
    if (this->contentViewGraphicsScene->getContentView()->getAppMode() != AppMode::Edit)
    {
        setAdditionalTransitionConditions->setEnabled(false);
        setName->setEnabled(false);
        deleteItem->setEnabled(false);
    }

    /* Connect QAction Signals to Arc Slots */
    connect(setName, SIGNAL(triggered()), this, SLOT(menuSetName()));
    connect(setAdditionalTransitionConditions, SIGNAL(triggered()), this, SLOT(menuSetAdditionalTransitionConditions()));
    connect(deleteItem, SIGNAL(triggered()), this, SLOT(menuDeleteItem()));

    /* Set the actions to the menu */
    menu->addAction(setName);
    menu->addAction(setAdditionalTransitionConditions);
    menu->addSeparator();
    menu->addAction(deleteItem);

    /* Open the context menu */
    menu->popup(contentView->viewport()->mapToGlobal(pos).toPoint());
}


void Transition::menuSetAdditionalTransitionConditions()
{
    QString text = "";
    if (additionalConditions != nullptr)
    {
        text = additionalConditions;
    }

    const auto qInputDialog = new TransitionAdditionalConditionsDialog(nullptr, text, globalFont);

    if (qInputDialog->exec() != QDialog::Accepted)
    {
        // Operation was aborted
        return;
    }

    additionalConditions = qInputDialog->textValue();

    updateTextView();
}

void Transition::menuSetName()
{

    const auto qInputDialog = new QInputDialog(nullptr);
    qInputDialog->setInputMode(QInputDialog::TextInput);
    qInputDialog->setWindowTitle("Name of Transition");
    qInputDialog->setLabelText("Name: ");
    qInputDialog->setFont(*globalFont);
    qInputDialog->setTextValue(name);
    qInputDialog->setTextEchoMode(QLineEdit::Normal);

    if (!qInputDialog->exec())
    {
        // Operation was aborted
        return;
    }

    name = qInputDialog->textValue();
    updateTextView();
}


void Transition::updateTextView()
{
    QString text;
    QTextStream stream(&text);

    if (!name.isNull() && !name.isEmpty())
    {
        stream << "Name: " << name << "<br>";
    }

    if (!additionalConditions.isNull())
    {
        stream << additionalConditions << "<br>";
    }

    delete qGraphicsTextItem;

    qGraphicsTextItem = new QGraphicsTextItem(this);
    qGraphicsTextItem->setFont(*globalFont);


    
    if (text.length()>0)
    {
        QString temp = text.left(text.length()-4);
        qGraphicsTextItem->setHtml("<div style='background:rgba(255, 255, 255, 60%);'>" + text.left(text.length() - 4) + "</div>");
    }

    qGraphicsTextItem->setPos((this->rect().width() - qGraphicsTextItem->boundingRect().width()) / 2, 20);

    qGraphicsTextItem->adjustSize();

    // Centers the Text
    QTextBlockFormat format;
    format.setAlignment(Qt::AlignCenter);
    QTextCursor cursor = qGraphicsTextItem->textCursor();
    cursor.select(QTextCursor::Document);
    cursor.mergeBlockFormat(format);
    cursor.clearSelection();
    qGraphicsTextItem->setTextCursor(cursor);
}

void Transition::menuDeleteItem()
{
    deleteAllConnectedArcs();
    delete this;
}

QVariant Transition::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if (change == ItemPositionChange && scene()) {
        moveLines();
    }
    else if (change == ItemSelectedChange) {
        if (this->contentViewGraphicsScene->getContentView()->getAppMode() == AppMode::Edit)
        {
            if (handleItemSelectedEvent(this, value))
            {
                return false;
            }
        }
        else if (this->contentViewGraphicsScene->getContentView()->getAppMode() == AppMode::Manual)
        {
            return false;
        }
    }
    return QGraphicsItem::itemChange(change, value);
}

void Transition::fontChanged() const
{
    if (qGraphicsTextItem != nullptr)
    {
	    qGraphicsTextItem->setFont(*globalFont);
        qGraphicsTextItem->setPos((this->rect().width() - qGraphicsTextItem->boundingRect().width()) / 2, 20);
    }
}

void Transition::unselectItem()
{
    this->setSelected(false);
}

bool Transition::couldTransitionFire(bool automatic) const
{
    const bool couldFire = std::all_of(this->endPointOfArcs.begin(), this->endPointOfArcs.end(), [automatic](const Arc* endPointOfArc) {
        return endPointOfArc->couldFire(automatic);
        });

    if (!couldFire)
        return false;

    return std::all_of(this->startPointOfArcs.begin(), this->startPointOfArcs.end(), [automatic](const Arc* startPointOfArc) {
			return startPointOfArc->couldFire(automatic);
        });
}

void Transition::reserveTokensForAutomaticRun() const
{
    for (const Arc* endPointOfArc : this->endPointOfArcs)
    {
        endPointOfArc->reserveTokensForAutomaticRun();
    }

    for (const Arc* startPointOfArc : this->startPointOfArcs)
    {
        startPointOfArc->reserveTokensForAutomaticRun();
    }

}

void Transition::colorTransitionForManualRun()
{
    if (couldTransitionFire(false))
    {
        itemPen->setColor(Qt::green);
    } else
    {
        itemPen->setColor(Qt::red);
    }
    this->setPen(*itemPen);

}

void Transition::resetColor()
{
    itemPen->setColor(Qt::black);
    this->setPen(*itemPen);
}

bool Transition::markTokensAndTransitionForAutomaticRun()
{
    // 1. Check if already marked as fireable
    if (automaticRunIsFireable)
        return false;

    // 2. Transition could fire (checks normal Transition conditions)
    if (!couldTransitionFire(true))
	    return false;

    // 3. Mark as fireable
    automaticRunIsFireable = true;

    // 4. Reserve Tokens in depending places
    reserveTokensForAutomaticRun();

    // 5. Mark Transition which have fired
    itemPen->setColor(Qt::blue);
    this->setPen(*itemPen);

    return true;
}

void Transition::fireTransition() const
{
    qInfo("Fire called");
    for (const Arc* endPointOfArc : this->endPointOfArcs)
    {
        endPointOfArc->fire();
    }

    for (const Arc* startPointOfArc : this->startPointOfArcs)
    {
        startPointOfArc->fire();
    }
}
