#pragma once

#include <QGraphicsPathItem>

#include <QPointF>
#include "Arc.fwd.h"


class ArcLinePath :
    public QGraphicsPathItem
{
public:
    ArcLinePath(Arc* parent, QLineF line); 
    bool prepareToAddKink(const QGraphicsSceneMouseEvent* event);
    void moveKinkToMouseCursor(const QGraphicsSceneMouseEvent* event);
    void mouseReleasedSaveKink(const QGraphicsSceneMouseEvent* event);
    void setStartPoint(QPointF start);
    void setEndPoint(QPointF end);
    void replaceKinks(const QList<QPointF>&);
    [[nodiscard]] QPointF getEndOfFirstLine() const;
    [[nodiscard]] QPointF getBeginOfLastLine() const;
    [[nodiscard]] QLineF getLastLine() const;

private:
    [[nodiscard]] int movePreviousAddedKink(QPointF mousePos) const;
    int addNewKink(QPointF mousePos);

    bool distancePointLine(QPointF& p, const QLineF& line, float distance) const;

    int indexOfSeparation = -1;
    Arc* arc;
};

