#pragma once

enum class PetriNetMainComponentType {
	Place, Transition
};
