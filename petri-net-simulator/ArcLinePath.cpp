#include "ArcLinePath.h"
#include "Arc.h"

ArcLinePath::ArcLinePath(Arc* parent, QLineF line): QGraphicsPathItem(static_cast<QGraphicsItem*>(parent))
{
	arc = parent;

	auto path = QPainterPath();
	path.moveTo(line.p1());
	path.lineTo(line.p2());

	this->setPath(path);

	this->setFlag(ItemIsSelectable, true);
	this->setFlag(ItemIsMovable, true);

	this->setPen(QPen(QBrush(Qt::GlobalColor::black), 3.0));
}

bool ArcLinePath::prepareToAddKink(const QGraphicsSceneMouseEvent* event)
{
	// Move Previous added kink
	indexOfSeparation = movePreviousAddedKink(event->scenePos());

	// If no kink was clicked maybe a normal line was clicked to add a new kink to this line
	if (indexOfSeparation == -1)
	{
		indexOfSeparation = addNewKink(event->scenePos());
	}
	
	return indexOfSeparation>=0;
}


void ArcLinePath::moveKinkToMouseCursor(const QGraphicsSceneMouseEvent* event)
{
	if (indexOfSeparation != -1)
	{
		QPainterPath path = this->path();
		path.setElementPositionAt(indexOfSeparation, event->scenePos().x(), event->scenePos().y());
		this->setPath(path);

		this->arc->updateView();
	}

}

void ArcLinePath::mouseReleasedSaveKink(const QGraphicsSceneMouseEvent* event)
{

	if (indexOfSeparation != -1)
	{
		QPainterPath path = this->path();
		path.setElementPositionAt(indexOfSeparation, event->scenePos().x(), event->scenePos().y());
		this->setPath(path);

		this->arc->updateView();

		indexOfSeparation = -1;
	}
}

void ArcLinePath::setStartPoint(QPointF start)
{
	QPainterPath path = this->path();
	path.setElementPositionAt(0, start.x(), start.y());
	this->setPath(path);
}

void ArcLinePath::setEndPoint(QPointF end)
{
	QPainterPath path = this->path();
	path.setElementPositionAt(path.elementCount()-1, end.x(), end.y());
	this->setPath(path);
}

QPointF ArcLinePath::getEndOfFirstLine() const
{
	const QPainterPath path = this->path();
	const auto element = path.elementAt(1);
	return {element.x, element.y};
}

QPointF ArcLinePath::getBeginOfLastLine() const
{
	const QPainterPath path = this->path();
	const auto element = path.elementAt(path.elementCount()-2);
	return {element.x, element.y};
}

QLineF ArcLinePath::getLastLine() const
{
	const QPainterPath path = this->path();
	const auto beginn = path.elementAt(path.elementCount() - 2);
	const auto end = path.elementAt(path.elementCount() - 1);
	return {QPointF(beginn.x, beginn.y), QPointF(end.x, end.y)};
}

void ArcLinePath::replaceKinks(const QList<QPointF>& kinks)
{
	const QPainterPath path = this->path();
	const auto start = path.elementAt(0);
	const auto end = path.elementAt(path.elementCount() - 1);

	QPainterPath newPath;
	newPath.moveTo(start);

	for (auto kink: kinks)
	{
		newPath.lineTo(kink);
	}

	newPath.lineTo(end);

	this->setPath(newPath);
}

int ArcLinePath::movePreviousAddedKink(QPointF mousePos) const
{

	// Endpunkt der ersten Linie bis Startpunkt der letzten linie
	for (int i = 1; i < this->path().elementCount() - 1; i++)
	{
		const QPainterPath::Element element = this->path().elementAt(i);
		QPointF point(element.x, element.y);

		const double distance = sqrt(pow(point.x() - mousePos.x(), 2) + pow(point.y() - mousePos.y(), 2));

		if (distance < 10)
		{
			return i;
		}
	}

	// mouse position is not in the near of a line point
	return -1;
}


int ArcLinePath::addNewKink(QPointF mousePos)
{
	int indexOfClickedPath = -1;
	const QPainterPath path = this->path();
	QPainterPath newPath;
	QPointF offset(0, 0);

	// add new kink
	for (int i = 0; i < path.elementCount(); i++)
	{
		QPainterPath::Element element = path.elementAt(i);
		QLineF line(offset, QPointF(element.x, element.y));

		constexpr float distance = 3;

		if (distancePointLine(mousePos, line, distance) &&
			element.type == QPainterPath::ElementType::LineToElement &&
			indexOfClickedPath == -1)
		{
			newPath.lineTo(mousePos.x(), mousePos.y());
			newPath.lineTo(element.x, element.y);

			indexOfClickedPath = i;
		}
		else {
			if (element.isLineTo()) {
				newPath.lineTo(element.x, element.y);
			}
			else if (element.isMoveTo()) {
				newPath.moveTo(element.x, element.y);
			}
			else {
				qWarning("Something is going wrong here");
			}
		}

		// Next Line will start at the end of previous one
		offset.rx() = element.x;
		offset.ry() = element.y;

	}

	if (indexOfClickedPath != -1)
	{
		this->setPath(newPath);
	}

	return indexOfClickedPath;
}

bool ArcLinePath::distancePointLine(QPointF& p, const QLineF& line, float maxDistance) const
{
	// Abstand Punkt - Gerade: https://de.serlo.org/mathe/2137/abstand-eines-punktes-zu-einer-geraden-berechnen-analytische-geometrie

	QPointF l1 = line.p1();
	QPointF l2 = line.p2();

	//Punkt
	double p1 = p.x();
	double p2 = p.y();

	//Verschiebung der Geraden
	double a1 = l1.x();
	double a2 = l1.y();

	//Steigung
	double b1 = l2.x() - l1.x();
	double b2 = l2.y() - l1.y();

	double c1 = p1 - a1;
	double c2 = p2 - a2;

	double kreuzprodukt3 = c1 * b2 - c2 * b1;

	double result = abs(kreuzprodukt3) / sqrt(pow(b1,2) + pow(b2,2));

	if (result < maxDistance)
		return true;
	return false;
}