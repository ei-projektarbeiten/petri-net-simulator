#include "AppState.h"

void AppState::componentSelectedForArc(QGraphicsItem*)
{
}

void AppState::setAppMode(AppMode newAppMode)
{
    this->appMode = newAppMode;
}

void AppState::setContentViewCursor(ContentViewCursor newContentViewCursor)
{
    this->contentViewCursor = newContentViewCursor;
}

ContentViewCursor AppState::getContentViewCursor()
{
    return ContentViewCursor();
}

AppMode AppState::getAppMode()
{
    return AppMode();
}
