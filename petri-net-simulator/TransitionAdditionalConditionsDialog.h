#pragma once
#include <QDialog>
#include <QLineEdit>

class TransitionAdditionalConditionsDialog :
    public QDialog
{
    Q_OBJECT
public:
    TransitionAdditionalConditionsDialog(QWidget* parent = nullptr, const QString& text = "", const QFont* font = new QFont());
    ~TransitionAdditionalConditionsDialog() override { delete le; }
    [[nodiscard]] QString textValue() const;


public slots:
    
    void okClicked();

private:

    QLineEdit* le;
};

