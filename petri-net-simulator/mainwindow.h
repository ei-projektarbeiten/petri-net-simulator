#pragma once

#include <QMainWindow>
#include <qsettings.h>
#include <QShortcut>

#include "AppState.h"
#include "ContentView.fwd.h"
#include "InOutputWindow.fwd.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void updateTitle();
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
    void inOutputWindowClosed();
    [[nodiscard]] int askUserForPermission(QString& text);

public slots:
    void deleteSelectedItem() const;

private slots:

    // File Button clicked
    void on_actionFileNew_triggered();
    void on_actionFileLoad_triggered();
    void on_actionFileSave_triggered();
    void on_actionFileSaveAs_triggered();

    // File Button clicked
    void on_actionSettingsChangeFontSize_triggered();
    void on_actionSettingsZoomIn_triggered() const;
    void on_actionSettingsZoomOut_triggered() const;

    // Mode Button clicked
    void on_actionModeEdit_triggered();
    void on_actionModeManual_triggered();
    void on_actionModeAutomatic_triggered();

    void on_actionSelect_triggered() const;

    // Edit Button clicked
    void on_actionEditPlace_triggered() const;
    void on_actionEditTransition_triggered() const;
    void on_actionEditArc_triggered() const;

    // Run Button clicked
    void on_actionRunExit_triggered();
    void on_actionRunAddMarker_triggered() const;
    void on_actionRunFireTransition_triggered() const;
    void on_actionRunAutoRun_triggered() const;

private:
    void loadGlobalFont();
    void setAppMode(AppMode newState);
    void fontChanged() const;

    Ui::MainWindow *ui;
    ContentView* contentView;
    InOutputWindow* inOutputWindow;

    QString filePath;

    // Global Shortcuts (which are not already defined in UI)
    QShortcut* deleteItem;
    QFont* globalFont;
    QSettings* mySettings;
};
