#pragma once

enum class AppMode {
    Edit, Manual, Automatic
};

