#pragma once

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include "ContentView.h"

#include "Place.fwd.h"
#include "Transition.fwd.h"
#include "Arc.fwd.h"
#include "PetriNetMainComponent.fwd.h"
#include "ArcLinePath.h"

class ContentViewGraphicsScene final : public QGraphicsScene
{
	Q_OBJECT

public:
	ContentViewGraphicsScene(ContentView *parent, QFont* globalFont, unsigned nextId);
	~ContentViewGraphicsScene() override;
	void updateTransitionColor();
	bool addArcToComponent(PetriNetMainComponent* item);
	void cursorWillBeChanged();
	void deleteSelectedItem();
	void removeArcFromList(Arc* arc);
	void removePlaceFromList(Place* place);
	void removeTransitionFromList(Transition* transition);
	void fontChanged();
	void updateMoveableStatus() const;
	void fireTransitionAutomatically(const QMap<QString, bool>& inputValues);
	QMap<QString, bool> evaluateOutputs();
	void replaceLists(const QList<Place*>&, const QList<Transition*>&, const QList<Arc*>&);
	void windowWasResized(QSize newSize);

	[[nodiscard]] ContentView* getContentView() const;
	[[nodiscard]] QList<QString> getOutputNames();
	[[nodiscard]] QList<QString> getInputNames();
	[[nodiscard]] QList<Place*>& getPlaces() { return places; }
	[[nodiscard]] QList<Transition*>& getTransitions() { return transitions; }
	[[nodiscard]] QList<Arc*>& getArcs() { return arcs; }
	[[nodiscard]] unsigned getNextId() const { return nextId; }

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

private:
	void leftButtonPressed(QGraphicsSceneMouseEvent* event);
	void addTransition(const QGraphicsSceneMouseEvent* event);
	void addPlace(const QGraphicsSceneMouseEvent* event);
	void checkForItemsToConnect(QGraphicsSceneMouseEvent* event);
	void searchForArcAtPosition(const QGraphicsSceneMouseEvent* event);

	void connectElementsWithArc(PetriNetMainComponent* startPoint, PetriNetMainComponent* endPoint);

	// Parent 
	ContentView* contentView;

	// children
	QList<Place*> places;
	QList<Transition*> transitions;
	QList<Arc*> arcs;

	PetriNetMainComponent* addArcStartPoint;

	ArcLinePath* selectedArcLine;
	QFont* globalFont;

	unsigned nextId;
};
