#include "SetOutputsForPlace.h"

#include <QDialogButtonBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QComboBox>
#include <QPushButton>

#include <algorithm>


SetOutputsForPlace::SetOutputsForPlace(QList<Output>& outputs, const QString& placeName, QFont* font): font(font)
{
    resize(450, 400);

    if (!placeName.isNull() && !placeName.isEmpty())
    {
        setWindowTitle("Set outputs for Place " + placeName);
    } else
    {
        setWindowTitle("Set outputs");
    }

    this->setAttribute(Qt::WA_QuitOnClose, false);

    const auto vbox = new QVBoxLayout;

    const auto description = new QLabel("Output with the user defined name will be true, when the token count in the place will fulfill the condition (sign + value) ");
    vbox->addWidget(description);

    vbox->addLayout(getLayoutWithAddAndTrashButtons());

    tableWidget = new QTableWidget(this);
    loadOutputsIntoTable(outputs);

	vbox->addWidget(tableWidget);

    const auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    buttonBox->setFont(*font);

    vbox->addWidget(buttonBox);

    this->setLayout(vbox);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &SetOutputsForPlace::okClicked);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

SetOutputsForPlace::~SetOutputsForPlace()
{
    delete tableWidget;
}

QList<Output> SetOutputsForPlace::getResult()
{
    return userDefinedOutputs;
}


void SetOutputsForPlace::okClicked()
{
    qInfo("OK Clicked");

    const QList<Output> temp;
    userDefinedOutputs = temp;

    for (int i = 0; i < tableWidget->rowCount(); i++)
    {
	    const auto nameEdit = dynamic_cast<QLineEdit*>(tableWidget->cellWidget(i, 0));
	    const auto commandEdit = dynamic_cast<QComboBox*>(tableWidget->cellWidget(i, 1));
	    const auto valueEdit = dynamic_cast<QLineEdit*>(tableWidget->cellWidget(i, 2));

        if (nameEdit->text() != "" && valueEdit->text() != "")
        {
            userDefinedOutputs.append(Output(nameEdit->text(), commandEdit->currentText(), valueEdit->text().toInt()));
        }
    }

    this->close();
    this->setResult(Accepted);
}

void SetOutputsForPlace::addClicked()
{
    tableWidget->insertRow(tableWidget->rowCount());

    const int rowIndex = tableWidget->rowCount() -1;

    const auto lineEditName = new QLineEdit();
    tableWidget->setCellWidget(rowIndex, 0, lineEditName);

    const auto combo = new QComboBox(this);
    combo->addItems(Output::getAllRelationalOperators());
    tableWidget->setCellWidget(rowIndex, 1, combo);

    const auto valueEdit = new QLineEdit();
    valueEdit->setValidator(new QIntValidator(0, 1000000, this));
    tableWidget->setCellWidget(rowIndex, 2, valueEdit);
}

void SetOutputsForPlace::trashClicked() const
{

    qInfo("Trash Clicked");

    const auto multipleRangess = tableWidget->selectedRanges();

    if (multipleRangess.count() == 0)
    {
	    if (tableWidget->rowCount()>0)
	    {
            tableWidget->removeRow(tableWidget->rowCount() - 1);
	    }
    } else
    {
        QSet<int> rowMarkedForDeletion;
	    for(int i = 0; i < multipleRangess.count(); i++)
	    {
            auto range = multipleRangess.at(i);

            if (range.rowCount()>0)
            {
	            for (int index2 = range.topRow(); index2 <= range.bottomRow(); index2++)
	            {
                    rowMarkedForDeletion.insert(index2);
	            }
            }
	    }
        QList<int> rowMarkedForDeletionList = rowMarkedForDeletion.values();
        std::sort(rowMarkedForDeletionList.rbegin(), rowMarkedForDeletionList.rend()); // reverse iterators

        for (int index = 0; index < rowMarkedForDeletionList.count(); index++)
        {
            qInfo("ID %d", rowMarkedForDeletionList.at(index));
            tableWidget->removeRow(rowMarkedForDeletionList.at(index));
        }
    }
}

void SetOutputsForPlace::loadOutputsIntoTable(QList<Output>& outputs)
{

    tableWidget->setFont(*font);
    tableWidget->setColumnCount(3);
    tableWidget->setRowCount(0);


    tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Name"));
    tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Sign"));
    tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Value"));


    if (outputs.count() == 0)
    {
        outputs.append(Output("Custom Name", "==", 0));
    }
    
    for (int i = 0; i < outputs.count(); i++)
    {
        auto output = outputs.at(i);

        addClicked();                               // F�gt eine Zeile hinzu

        const auto nameEdit = dynamic_cast<QLineEdit*>(tableWidget->cellWidget(i, 0));
        nameEdit->setText(output.getName());

        const auto commandEdit = dynamic_cast<QComboBox*>(tableWidget->cellWidget(i, 1));
        commandEdit->setCurrentIndex(output.getRelationalOperatorIndex());

        const auto valueEdit = dynamic_cast<QLineEdit*>(tableWidget->cellWidget(i, 2));
        valueEdit->setText(QString::number(output.getValue()));
    }

    tableWidget->setColumnWidth(0, 200);
    
}

QHBoxLayout* SetOutputsForPlace::getLayoutWithAddAndTrashButtons()
{
	const auto iconRow = new QHBoxLayout;
    iconRow->QLayoutItem::setAlignment(Qt::AlignLeft);

	const auto addPushButton = new QPushButton(this);
    addPushButton->setIcon(QIcon(QString::fromUtf8(":/images/add-fill.png")));
    addPushButton->setFont(*font);
    addPushButton->setIconSize(QSize(32, 32));
    connect(addPushButton, &QPushButton::clicked, this, &SetOutputsForPlace::addClicked);

    // add the button to a layout here
    iconRow->addWidget(addPushButton);

	const auto iconTrashButton = new QPushButton(this);
    iconTrashButton->setIcon(QIcon(QString::fromUtf8(":/images/delete-bin-fill.png")));
    iconTrashButton->setFont(*font);
    iconTrashButton->setIconSize(QSize(32, 32));
    connect(iconTrashButton, &QPushButton::clicked, this, &SetOutputsForPlace::trashClicked);

    // add the button to a layout here
    iconRow->addWidget(iconTrashButton);

    return iconRow;
}