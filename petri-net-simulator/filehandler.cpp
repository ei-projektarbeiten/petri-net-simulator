#include "FileHandler.h"

#include <QJsonObject>
#include <QJsonArray>
#include <qjsondocument.h>

#include "Place.h"


QString FileHandler::contentViewToJson(const ContentView* contentView)
{
    QJsonObject rootObj;
    const QJsonObject windowObject {
    	{"width", contentView->width()},
        {"height", contentView->height()}
    };

    rootObj.insert("window", windowObject);

    const auto graphicsScene = contentView->getContentViewGraphicsScene();

    rootObj.insert("nextId", static_cast<int>(graphicsScene->getNextId()));

    auto places = graphicsScene->getPlaces();
	QJsonArray placeArray;
    for (const auto place: places)
    {
        QJsonObject position {
            {"x", place->scenePos().x()},
            {"y", place->scenePos().y()}
        };
        QJsonObject placeObject;
        placeObject.insert("id", static_cast<int>(place->getId()));
        placeObject.insert("tokens", static_cast<int>(place->getTokenCount()));
        placeObject.insert("position", position);
        placeObject.insert("capacity", place->getCapacity());
        placeObject.insert("name", place->getName());

        QJsonArray outputs;
        for (auto output: place->getOutputs())
        {
            QJsonObject jsonOutput{
            {"name", output.getName()},
			{"relationalOperator", output.getRelationalOperator()},
            {"value", static_cast<int>(output.getValue())}
            };
            outputs.append(jsonOutput);
        }
        placeObject.insert("outputs", outputs);

        placeArray.append(placeObject);
    }

    rootObj.insert("places", placeArray);


    auto transitions = graphicsScene->getTransitions();
    QJsonArray transitionArray;
    for (const auto transition : transitions)
    {
        QJsonObject position{
            {"x", transition->scenePos().x()},
            {"y", transition->scenePos().y()}
        };
        QJsonObject transitionObject;
        transitionObject.insert("id", static_cast<int>(transition->getId()));
        transitionObject.insert("name", transition->getName());
        transitionObject.insert("additionalConditions", transition->getAdditonalConditions());
        transitionObject.insert("position", position);
        
        transitionArray.append(transitionObject);
    }
    rootObj.insert("transitions", transitionArray);

    auto arcs = graphicsScene->getArcs();
    QJsonArray arcsArray;
    for (const auto arc : arcs)
    {
        
        QJsonObject arcObject;
        arcObject.insert("id", static_cast<int>(arc->getId()));
        arcObject.insert("inhibitor", arc->isInhibitorArc());
        arcObject.insert("multiplicity", static_cast<int>(arc->getMultiplicity()));
        arcObject.insert("fromId", static_cast<int>(arc->getStartPoint()->getId()));
        arcObject.insert("toId", static_cast<int>(arc->getEndPoint()->getId()));

        QJsonArray additionalPoints;
            
        auto path = arc->getArcLinePath()->path();
        
        for (int i = 1; i < path.elementCount() - 1; i++ )
        {
            QJsonObject position{
            //{"nr", i},
            {"x", path.elementAt(i).x},
            {"y", path.elementAt(i).y}
            };
            
            additionalPoints.append(position);
        }
        arcObject.insert("additionalPoints", additionalPoints);

        arcsArray.append(arcObject);
    }

    rootObj.insert("arcs", arcsArray);

    const QJsonDocument doc(rootObj);
    return doc.toJson(QJsonDocument::Indented);
}

ContentView* FileHandler::jsonToContentViewPtr(MainWindow* mainWindow, QFont* font, const QByteArray& json)
{

    QJsonParseError errorPtr;
    QJsonDocument doc = QJsonDocument::fromJson(json, &errorPtr);
    if (doc.isNull()) {
        qDebug() << "Parse failed";
        return nullptr;
    }

    QJsonObject rootObj = doc.object();

    if (!rootObj.value("window").isObject())
    {
        qDebug() << "Does not contain window object";
        return nullptr;
    }

    auto nextId = rootObj.value("nextId").toInt();
    auto contentView = new ContentView(mainWindow, ContentViewCursor::Select, AppMode::Edit, font, nextId);

    ContentViewGraphicsScene* contentViewGraphics = contentView->getContentViewGraphicsScene();

	//auto window = rootObj.value("window").toObject();
    //contentView->setFixedWidth(window.value("width").toInt());
    //contentView->setFixedHeight(window.value("height").toInt());

    QMap<int, PetriNetMainComponent*> mainComponents;
    QList<Arc*> arcs;
    QList<Place*> places;
    QList<Transition*> transitions;

    // 1. Places

    QJsonArray placesArray = rootObj.value("places").toArray();

    foreach(const QJsonValue & val, placesArray) {
        const int id = val.toObject().value("id").toInt();
        const int tokens = val.toObject().value("tokens").toInt();
        const int capacity = val.toObject().value("capacity").toInt();
        const QString name = val.toObject().value("name").toString();

        const QJsonObject& position = val.toObject().value("position").toObject();
        const double x = position.value("x").toDouble();
        const double y = position.value("y").toDouble();

        const QJsonArray& outputs = val.toObject().value("outputs").toArray();

        QList<Output> placeOutputs;
        foreach(const QJsonValue & output, outputs) {
            const QString outputName = output.toObject().value("name").toString();
            const QString relationalOperator = output.toObject().value("relationalOperator").toString();
            const unsigned value = output.toObject().value("value").toInt();
            placeOutputs.append({ outputName, relationalOperator, value });
        }

        auto place = new Place(contentViewGraphics, font, id, tokens, capacity, name, placeOutputs);
        
        contentViewGraphics->addItem(place);

        place->setPos({ x, y });

        places.append(place);
        mainComponents.insert(id, place);
    }

    // 2. Transitions

    QJsonArray transitionsArray = rootObj.value("transitions").toArray();

    foreach(const QJsonValue& val, transitionsArray) {
        const int id = val.toObject().value("id").toInt();
        const QString name = val.toObject().value("name").toString();
        const QString additionalConditions = val.toObject().value("additionalConditions").toString();

        const QJsonObject& position = val.toObject().value("position").toObject();
        const double x = position.value("x").toDouble();
        const double y = position.value("y").toDouble();

        auto transition = new Transition(contentViewGraphics, font, id, name, additionalConditions);
        
        contentViewGraphics->addItem(transition);

        transition->setPos({ x, y });

        transitions.append(transition);
        mainComponents.insert(id, transition);
    }

    // 3. Arcs

    QJsonArray arcsArray = rootObj.value("arcs").toArray();

    foreach(const QJsonValue& val, arcsArray) {
        const int id = val.toObject().value("id").toInt();
        const bool inhibitor = val.toObject().value("inhibitor").toBool();
        const int multiplicity = val.toObject().value("multiplicity").toInt();
        const int fromId = val.toObject().value("fromId").toInt();
        const int toId = val.toObject().value("toId").toInt();

        const QJsonArray& additionalPoints = val.toObject().value("additionalPoints").toArray();

        QList<QPointF> arcAdditionalPoints;
        foreach(const QJsonValue & additionalPoint, additionalPoints) {
            const double x = additionalPoint.toObject().value("x").toDouble();
            const double y = additionalPoint.toObject().value("y").toDouble();

            arcAdditionalPoints.append({ x,y });
        }


        auto startPoint = mainComponents.value(fromId);
        auto endPoint = mainComponents.value(toId);

        auto arc = new Arc(contentViewGraphics, font, id, inhibitor, multiplicity, startPoint, endPoint);
        contentViewGraphics->addItem(arc);

        arc->drawLineWithKinks(arcAdditionalPoints);

        arcs.append(arc);
    }

    contentViewGraphics->replaceLists(places, transitions, arcs);
    return contentView;
}
