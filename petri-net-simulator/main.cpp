#include "MainWindow.h"

#include <QApplication>
#include <QLoggingCategory>

int main(int argc, char *argv[])
{
    // disables Debug output
    QLoggingCategory::setFilterRules("*.debug=false");
    //QLoggingCategory::setFilterRules("*.info=false");
    //QLoggingCategory::setFilterRules("*.warning=false");
    //QLoggingCategory::setFilterRules("*.critical=false");

    QApplication a(argc, argv);
    MainWindow w;
	w.show();
    return a.exec();
}
