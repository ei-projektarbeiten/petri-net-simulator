#include "ContentViewGraphicsScene.h"

#include "Place.h"
#include "Arc.h"
#include "InputStringHelper.h"
#include "Transition.h"
#include "PetriNetMainComponent.h"

ContentViewGraphicsScene::ContentViewGraphicsScene(ContentView *parent, QFont* globalFont, unsigned nextId)
	: QGraphicsScene(parent)
{
    this->contentView = parent;

    this->globalFont = globalFont;

    this->addArcStartPoint = nullptr;
    this->selectedArcLine = nullptr;

    this->nextId = nextId;

    this->setFont(*globalFont);
}

ContentViewGraphicsScene::~ContentViewGraphicsScene()
{
    
	//delete addArcStartPoint;


	while (!arcs.empty()) {
		const Arc* arc = arcs.front();
        
        // Done in Arcs destructor
        // arcs.pop_front();
        
        delete arc;
    }

    while (!places.empty()) {
	    const Place* place = places.front();

        // Done in Place destructor
        // places.pop_front();

        delete place;
    }

    while (!transitions.empty()) {
	    const Transition* transition = transitions.front();

        // Done in Transition destructor
        // transitions.pop_front();

        delete transition;
    }
}

void ContentViewGraphicsScene::replaceLists(const QList<Place*>& newPlaces, const QList<Transition*>& newTransitions, const QList<Arc*>& newArcs)
{
    this->places = newPlaces;
    this->transitions = newTransitions;
    this->arcs = newArcs;
}

void ContentViewGraphicsScene::windowWasResized(QSize newSize)
{
    int minWidth = 0;
    int minHeight = 0;

    for(auto place: places)
    {
        if (place->x() > minWidth)
            minWidth = place->x();

        if (place->y() > minHeight)
            minHeight = place->y();
    }

    for (auto transition : transitions)
    {
        if (transition->x() > minWidth)
            minWidth = transition->x();

        if (transition->y() > minHeight)
            minHeight = transition->y();
    }

    for (auto arc : arcs)
    {
        if (arc->x() + arc->boundingRect().width() > minWidth)
            minWidth = arc->x() + arc->boundingRect().width();

        if (arc->y() + arc->boundingRect().height() > minHeight)
            minHeight = arc->y() + arc->boundingRect().height();
    }

    if (newSize.width() > minWidth)
    {
        minWidth = newSize.width();
    }

    if (newSize.height() > minHeight)
    {
        minHeight = newSize.height();
    }

    this->setSceneRect(0, 0, minWidth + 200, minHeight + 200);
}

void ContentViewGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    qInfo("ContentViewGraphicsScene: Mouse PressEvent");

    this->selectedArcLine = nullptr;

    const auto contentViewCursor = this->contentView->getContentViewCursor();

    if (event->button() == Qt::RightButton && contentViewCursor == ContentViewCursor::AddToken) 
    {
        bool clickedOnPlace = false;

        for (const Place* place : this->places)
        {
	        if (place->boundingRect().contains(event->scenePos() - place->pos()))
	        {
                clickedOnPlace = true;
                break;
	        }
        }
        if (clickedOnPlace)
        {
            QGraphicsScene::mousePressEvent(event);
        }
        else
        {
            this->contentView->setCurrentCursor(ContentViewCursor::Select);
        }
    }
    else if (event->button() == Qt::RightButton) 
    {
        if (contentViewCursor == ContentViewCursor::Select)
        {
            QGraphicsScene::mousePressEvent(event);
        } else
        {
            this->contentView->setCurrentCursor(ContentViewCursor::Select);
        }
    }
    else if (event->button() == Qt::LeftButton) 
    {
        this->leftButtonPressed(event);
    }
}

void ContentViewGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    qDebug("mouseMoveEvent");
    QGraphicsScene::mouseMoveEvent(event);

    if (selectedArcLine != nullptr)
    {
        selectedArcLine->moveKinkToMouseCursor(event);
    }
}

void ContentViewGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    qInfo("mouseReleaseEvent");
    QGraphicsScene::mouseReleaseEvent(event);

    if (selectedArcLine != nullptr)
    {
        selectedArcLine->mouseReleasedSaveKink(event);

        this->selectedArcLine = nullptr;
    }
}

void ContentViewGraphicsScene::leftButtonPressed(QGraphicsSceneMouseEvent* event)
{

    qInfo("Pos: %f / %f", event->pos().x(), event->pos().y());

    switch (this->contentView->getContentViewCursor()) {
    case ContentViewCursor::Select:
        if (this->contentView->getAppMode() == AppMode::Edit)
        {
            searchForArcAtPosition(event);
        }

    	QGraphicsScene::mousePressEvent(event);
        break;

    case ContentViewCursor::Place:
        addPlace(event);
        break;

    case ContentViewCursor::Transition:
        addTransition(event);
        break;

    case ContentViewCursor::Arc:
        checkForItemsToConnect(event);
        break;

    case ContentViewCursor::AddToken:
    case ContentViewCursor::FireTransition:
        QGraphicsScene::mousePressEvent(event);
        break;
    }

    windowWasResized(QSize());
}

// Adds Transition to the Scene
void ContentViewGraphicsScene::addTransition(const QGraphicsSceneMouseEvent* event)
{
    nextId++;
	const auto transition = new Transition(this, nextId, globalFont);
    this->addItem(transition);

    transition->setPos(event->scenePos());
    this->transitions.push_back(transition);

}

// Adds Place to the Scene
void ContentViewGraphicsScene::addPlace(const QGraphicsSceneMouseEvent* event)
{
    nextId++;
	const auto place = new Place(this, nextId, globalFont);
    this->addItem(place);

    const auto pos = event->scenePos();
    place->setPos(pos);
    this->places.push_back(place);
}

// Searches for a Item at the clicked Position
void ContentViewGraphicsScene::checkForItemsToConnect(QGraphicsSceneMouseEvent* event)
{
	const QGraphicsItem* foundItem = itemAt(event->scenePos(), this->contentView->transform());
    if (foundItem != nullptr) {
        qInfo("found item");
        QGraphicsScene::mousePressEvent(event);
    }
    else {
        qInfo("not found item");

    }    
}

void ContentViewGraphicsScene::searchForArcAtPosition(const QGraphicsSceneMouseEvent* event)
{
    QGraphicsItem* item = itemAt(event->scenePos(), this->contentView->transform());

    const auto arcLine = dynamic_cast<ArcLinePath*>(item);

    if (arcLine != nullptr) {
        this->selectedArcLine = arcLine;
        const bool successful = arcLine->prepareToAddKink(event);
        if (!successful)
        {
            this->selectedArcLine = nullptr;
        }
    }
}

/*return value is true when component can be connected*/
bool ContentViewGraphicsScene::addArcToComponent(PetriNetMainComponent* item)
{

    if (this->addArcStartPoint == nullptr) 
    {
        this->addArcStartPoint = item;
        return true;
    }
    
    if (this->addArcStartPoint->getPetriNetMainComponentType() != item->getPetriNetMainComponentType()) 
    {
        for (const auto& arc: this->addArcStartPoint->getStartPointOfArcs())
        {
	        if (arc->getEndPoint() == item)
	        {
                qInfo("Such a arc already exists");
                this->addArcStartPoint = item;
                return false;
	        }
        }

        this->connectElementsWithArc(this->addArcStartPoint, item);
        this->addArcStartPoint = nullptr;
        return true;
    }
    

    qInfo("Items are from the same type");
    this->addArcStartPoint = item;
    return false;
    
}

// Reset settings to start
void ContentViewGraphicsScene::cursorWillBeChanged()
{
    this->addArcStartPoint = nullptr;

    updateMoveableStatus();
}

void ContentViewGraphicsScene::deleteSelectedItem()
{
    qInfo("Delete Selected Items");

    // Delete Selected Arcs first
    auto selectedItems = this->selectedItems();
    for (QGraphicsItem* item : qAsConst(selectedItems)) {
	    const Arc* arc = dynamic_cast<Arc*>(item);

        if (arc != nullptr) {
            this->removeItem(item);
            delete item;
        }
    }

    // Delete Selected PetriNetMainComponent
    selectedItems = this->selectedItems();
    for (QGraphicsItem* item : qAsConst(selectedItems)) {
	    const auto mainComponent = dynamic_cast<PetriNetMainComponent*>(item);

        if (mainComponent != nullptr) {
            mainComponent->deleteAllConnectedArcs();

            this->removeItem(item);
            delete item;

        }
    }
}

void ContentViewGraphicsScene::removeArcFromList(Arc * arc)
{
    this->arcs.removeOne(arc);
}

void ContentViewGraphicsScene::removePlaceFromList(Place* place)
{
    this->places.removeOne(place);
}

void ContentViewGraphicsScene::removeTransitionFromList(Transition* transition)
{
    this->transitions.removeOne(transition);
}

void ContentViewGraphicsScene::fontChanged()
{
    this->setFont(*globalFont);

    for (Arc* arc: arcs) {
        arc->fontChanged();
    }

    for (Place* place : places) {
        place->fontChanged();
    }

    for (const Transition* transition : transitions) {
        transition->fontChanged();
    }

}

void ContentViewGraphicsScene::updateMoveableStatus() const
{
    if (this->contentView->getAppMode() == AppMode::Edit && this->contentView->getContentViewCursor() == ContentViewCursor::Select)
    {
        // enable Edit
        for (Place* place : this->places)
        {
            place->setFlag(QGraphicsItem::ItemIsMovable, true);
        }

        for (Transition* transition : this->transitions)
        {
            transition->setFlag(QGraphicsItem::ItemIsMovable, true);
        }
    }
    else
    {
        // disable Edit
        for (Place* place : this->places)
        {
            place->setFlag(QGraphicsItem::ItemIsMovable, false);
        }

        for (Transition* transition : this->transitions)
        {
            transition->setFlag(QGraphicsItem::ItemIsMovable, false);
        }
    }
	
}

QList<QString> ContentViewGraphicsScene::getOutputNames()
{
    QSet<QString> outputNames;
    for (Place* place : places) {
        for (Output output: place->getOutputs())
        {
            outputNames.insert(output.getName());
        }
    }
    auto list = outputNames.values();
    list.sort();
    return list;
}

QList<QString> ContentViewGraphicsScene::getInputNames()
{
    QSet<QString> inputNames;
    for (Transition* transition : transitions) {
        QString additionalConditions = transition->getAdditonalConditions();
        for (QString& name: InputStringHelper::extractInputNames(additionalConditions))
        {
            inputNames.insert(name);
        }
    }
    auto list = inputNames.values();
    list.sort();
    return list;
}

void ContentViewGraphicsScene::fireTransitionAutomatically(const QMap<QString, bool>& inputValues)
{

    // 0. Reset Color of Transitions --> from last automatic run

    for (Transition* transition : this->transitions)
    {
        transition->resetColor();
    }

    
    // 1. evaluate Inputs --> for next step just consider transitions where input are fulfilled

    QList<Transition*> transitionsWithInputsFulfilled;
	for (const auto transition : transitions)
    {

        auto additionalConditions = transition->getAdditonalConditions();

        if (InputStringHelper::areInputConditionsFulfilled(additionalConditions, inputValues))
            transitionsWithInputsFulfilled.append(transition);
    }

    // 2. mark Transitions where normal (inhibitor and normal arc) and "input" Conditions are fulfilled

	unsigned countFireTransitions = 0;
    while (true)
    {
	    const unsigned oldCount = countFireTransitions;

        for (const auto transition : transitionsWithInputsFulfilled)
        {
            // Transition kann jetzt feuern --> Alle Transitionen m�ssen nochmal durchlaufen werden --> vllt geht ja jetzt eine, die davor nicht ging
            if (transition->markTokensAndTransitionForAutomaticRun())
                countFireTransitions++;
        }

        if (countFireTransitions == oldCount)
            break;
    }

    //3. Set Tokens in places --> tokens = countTokensWillBeAdded - countTokensWillBeReduced + tokens

	for (const auto place : places)
    {
        place->fireAutomaticUpdateTokenCount();
    }

	for (const auto transition: transitionsWithInputsFulfilled)
	{
        transition->resetAutomaticRun();
	}
}

QMap<QString, bool> ContentViewGraphicsScene::evaluateOutputs()
{
    QMap<QString, bool> outputList;

    for (const auto place : places)
    {
        const QList<Output>& outputs = place->getOutputs();
        const unsigned tokenCount = place->getTokenCount();

        for(auto output: outputs)
        {
            bool result = false;
            if (output.getRelationalOperator() == "==")
            {
                result = tokenCount == output.getValue();
            }
            else if (output.getRelationalOperator() == "!=")
            {
                result = tokenCount != output.getValue();
            }
            else if (output.getRelationalOperator() == ">")
            {
                result = tokenCount > output.getValue();
            }
            else if (output.getRelationalOperator() == ">=")
            {
                result = tokenCount >= output.getValue();
            }
            else if (output.getRelationalOperator() == "<")
            {
                result = tokenCount < output.getValue();
            }
            else if (output.getRelationalOperator() == "<=")
            {
                result = tokenCount <= output.getValue();
            }

            if (outputList.contains(output.getName()))
            {
                result = outputList.value(output.getName(), true) && result;
                outputList.remove(output.getName());
            }

            outputList.insert(output.getName(), result);
        }
    }

    return outputList;
}

void ContentViewGraphicsScene::connectElementsWithArc(PetriNetMainComponent* startPoint, PetriNetMainComponent* endPoint)
{
    nextId++;
	const auto arc = new Arc(this, nextId, startPoint, endPoint, globalFont);
    this->addItem(arc);

    arc->drawLine();

    this->arcs.push_back(arc);
    qInfo("Two components connected");

    this->clearSelection();
    this->clearFocus();
}


ContentView* ContentViewGraphicsScene::getContentView() const
{
    return this->contentView;
}

void ContentViewGraphicsScene::updateTransitionColor()
{
    if (this->contentView->getAppMode() == AppMode::Manual)
    {
        for (Transition* transition : this->transitions)
        {
            transition->colorTransitionForManualRun();
        }
    }
    else
    {
        for (Transition* transition : this->transitions)
        {
            transition->resetColor();
        }
    }
	
}