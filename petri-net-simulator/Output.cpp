#include "Output.h"

long long Output::getRelationalOperatorIndex() const
{
    return allRelationalOperators.indexOf(relationalOperator);
}
