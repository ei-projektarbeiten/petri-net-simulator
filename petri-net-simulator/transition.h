#pragma once

#include <QGraphicsRectItem>

#include "ContentViewGraphicsScene.fwd.h"
#include "PetriNetMainComponent.h"

class Transition: QObject, public QGraphicsRectItem, public PetriNetMainComponent
{
    Q_OBJECT

public:
    ~Transition() override;
    Transition(ContentViewGraphicsScene*, unsigned, QFont*);
    Transition(ContentViewGraphicsScene*, QFont*, unsigned, const QString&, const QString&);
    void fontChanged() const;
    void colorTransitionForManualRun();
    void resetColor();
    const QString& getAdditonalConditions() { return additionalConditions; }
    bool markTokensAndTransitionForAutomaticRun();
    void resetAutomaticRun() { automaticRunIsFireable = false; }
    QGraphicsItem* getQGraphicsItem() override { return this; }

public slots:
    void menuSetName();
    void updateTextView();
    void menuDeleteItem();
    void menuSetAdditionalTransitionConditions();


protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
    void unselectItem() override;
    


private:
    void openContextMenu(QPointF pos);
    void fireTransition() const;
    bool couldTransitionFire(bool automatic) const;
    void reserveTokensForAutomaticRun() const;

    ContentViewGraphicsScene* contentViewGraphicsScene;
    QGraphicsTextItem* qGraphicsTextItem;

    QString additionalConditions;
    
    bool automaticRunIsFireable;
};
