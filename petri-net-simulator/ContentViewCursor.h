#pragma once

enum class ContentViewCursor {
    Select, Place, Transition, Arc, AddToken, FireTransition
};
