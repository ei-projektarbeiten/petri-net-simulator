#pragma once

#include "Transition.h"

class InputStringHelper
{

public:
    static QList<QString> extractInputNames(const QString& text);
    
    static bool checkSyntax(const QString& text);
    static bool areInputConditionsFulfilled(const QString& inputCondition, const QMap<QString, bool>& inputValues);

private:
    static QStringList splitInputConditionsIntoList(const QString& text);
    static bool checkForConjunction(const QStringList& list, int i);
    static bool checkForNegationOrText(const QStringList& list, int i);
    InputStringHelper() = default;
    
};

