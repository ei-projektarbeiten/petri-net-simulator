#pragma once

#include <QGraphicsLineItem>
#include <QPolygonF>
#include <QInputDialog>
#include <QGraphicsItemGroup>

#include "ContentViewGraphicsScene.h"
#include "PetriNetMainComponent.fwd.h"
#include "ArcLinePath.h"

class Arc: public QObject, public QGraphicsItemGroup
{
    Q_OBJECT

public:
    ~Arc() override;
    Arc(ContentViewGraphicsScene* contentViewGraphicsScene, unsigned id, PetriNetMainComponent* startPoint, PetriNetMainComponent* endPoint, QFont* globalFont);
    Arc(ContentViewGraphicsScene* contentViewGraphics, QFont* font, int id, bool inhibitor, int multiplicity, PetriNetMainComponent* startPoint, PetriNetMainComponent* endPoint);

    void updateView();
    void drawLine();

    void fontChanged();

    void fire() const;

    void reserveTokensForAutomaticRun() const;

    void drawLineWithKinks(const QList<QPointF>&);

    [[nodiscard]] bool couldFire(bool automatic) const;

    [[nodiscard]] PetriNetMainComponent* getEndPoint() const { return endPoint; }
    [[nodiscard]] PetriNetMainComponent* getStartPoint() const { return startPoint; }
    [[nodiscard]] unsigned getId() const { return id; }
    [[nodiscard]] ArcLinePath* getArcLinePath() const { return qGraphicsPathItem; }
    [[nodiscard]] bool isInhibitorArc() const { return inhibitorArc; }
    [[nodiscard]] unsigned getMultiplicity() const { return multiplicity; }

public slots:
    void menuChangeType();
    void menuShowMultiplicityDialog();
    void menuDeleteItem() const;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

private:
    void internalMoveStartPoint(QPointF newCenter) const;
    void internalMoveEndPoint(QPointF newCenter) const;

    static QPolygonF getArrow(QLineF);
    void moveItems(QLineF);
    void showMultiplicity();
    double calculateOffsetDependingOnType(PetriNetMainComponent* component, QLineF lineShowToTheComponent) const;

    static QPointF getPositionForInhibitorCircle(QLineF line, double radius);
    

    bool inhibitorArc = false;
    unsigned int multiplicity = 1;

    PetriNetMainComponent* startPoint;
    PetriNetMainComponent* endPoint;

    ContentViewGraphicsScene* contentViewGraphicsScene;
    
    ArcLinePath* qGraphicsPathItem;
    QGraphicsPolygonItem* qGraphicsArrowItem;
    QGraphicsEllipseItem* qGraphicsInhibitorItem;
    QGraphicsTextItem* multiplicityTextItem;

    QFont* globalFont;
    unsigned id;
};

