#pragma once

#include "Arc.h"
#include "PetriNetMainComponentType.h"

#include <QGraphicsItem>

#include <list>

class PetriNetMainComponent
{
public:
	virtual ~PetriNetMainComponent();
	PetriNetMainComponent(unsigned id, PetriNetMainComponentType type, QFont* globalFont): itemPen(nullptr),
		globalFont(globalFont), type(type), id(id) {}


	virtual QGraphicsItem* getQGraphicsItem() = 0;
	void addArcAsStartPoint(Arc* newStartArc);
	void addArcAsEndPoint(Arc* newEndArc);
	void removeStartPointArc(Arc* deletableArc);
	void removeEndPointArc(Arc* deletableArc);
	void deleteAllConnectedArcs();

	[[nodiscard]] PetriNetMainComponentType getPetriNetMainComponentType() const;

	[[nodiscard]] unsigned getId() const { return id; }
	[[nodiscard]] QString getName() { return name; }

	[[nodiscard]] const std::list<Arc*>& getStartPointOfArcs() { return startPointOfArcs; }

protected:
	void moveLines() const;
	bool handleItemSelectedEvent(QAbstractGraphicsShapeItem* qGraphicsItem, const QVariant& value);
	virtual void unselectItem() = 0;

	/**
	 * \brief These arcs start at this main component
	 */
	std::list<Arc *> startPointOfArcs;

	/**
	 * \brief These arcs end at this main component
	 */
	std::list<Arc *> endPointOfArcs;
	QPen* itemPen;
	QFont* globalFont;

	long long lastAddedArcUnixMs = 0;
	QString name;

private:
	PetriNetMainComponentType type;
	unsigned id;
	
};

