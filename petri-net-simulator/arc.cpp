#include "Arc.h"

#include "PetriNetMainComponent.h"

#include <cmath>
#include <QMenu>

#include "Place.h"

constexpr double PI = 3.14159265358979323846;

Arc::~Arc()
{
	this->startPoint->removeStartPointArc(this);
	this->endPoint->removeEndPointArc(this);

	this->contentViewGraphicsScene->removeArcFromList(this);
	this->contentViewGraphicsScene = nullptr;

	// delete qGraphicsPathItem;	// deleted by QT
	// delete qGraphicsArrowItem;	// deleted by QT
	// delete qGraphicsInhibitorItem;	// deleted by QT


	//delete multiplicityTextItem; // deleted by QT
}

//Folgt diesem Beispiel: https://stackoverflow.com/questions/32192607/how-to-use-itemchange-from-qgraphicsitem-in-qt/32198716#32198716
Arc::Arc(ContentViewGraphicsScene* contentViewGraphicsScene,
	unsigned id,
	PetriNetMainComponent* startPoint, 
	PetriNetMainComponent* endPoint, QFont* globalFont): QObject(contentViewGraphicsScene), QGraphicsItemGroup()
{
	setHandlesChildEvents(true);

	this->id = id;
	this->startPoint = startPoint;
	this->endPoint = endPoint;
	this->contentViewGraphicsScene = contentViewGraphicsScene;
	this->globalFont = globalFont;

	this->qGraphicsArrowItem = nullptr;
	this->qGraphicsInhibitorItem = nullptr;
	this->qGraphicsPathItem = nullptr;
	this->multiplicityTextItem = nullptr;
}

Arc::Arc(ContentViewGraphicsScene* contentViewGraphics, QFont* font, int id, bool inhibitor, int multiplicity,
	PetriNetMainComponent* startPoint, PetriNetMainComponent* endPoint): Arc(contentViewGraphics, id, startPoint, endPoint, font)
{
	this->inhibitorArc = inhibitor;
	this->multiplicity = multiplicity;

}

void Arc::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	qInfo("Arc Clicked ");

	if (event->button() == Qt::RightButton
		&& this->contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::Select
		//&& 
		&& this->boundingRect().contains(event->scenePos())) 
	{
		qInfo("Arc Right Clicked -> Open Menu or Dialog");

		// open Menu

		const auto contentView = this->contentViewGraphicsScene->getContentView();

		/* Create an object context menu */
		const auto menu = new QMenu(contentView);
		menu->setFont(*globalFont);

		/* Create actions to the context menu */
		const auto typeAction = new QAction("", this);
		if (inhibitorArc) {
			typeAction->setText("Change to normal Arc");
		}
		else {
			typeAction->setText("Change to inhibitor Arc");
			if (endPoint->getPetriNetMainComponentType() == PetriNetMainComponentType::Place) {
				// From Transition to Place Inhibitor Arcs are prohibited
				typeAction->setEnabled(false);
			}
		}
		const auto multiplicityAction = new QAction("change multiplicity", this);
		const auto deleteAction = new QAction("delete", this);

		if (inhibitorArc)
		{
			multiplicityAction->setEnabled(false);
		}

		if (this->contentViewGraphicsScene->getContentView()->getAppMode() != AppMode::Edit)
		{
			typeAction->setEnabled(false);
			multiplicityAction->setEnabled(false);
			deleteAction->setEnabled(false);
		}

		// Just added here that the Shortcut is displayed in the context menu
		deleteAction->setShortcut(Qt::Key_Delete);

		/* Connect QAction Signals to Arc Slots */
		connect(typeAction, SIGNAL(triggered()), this, SLOT(menuChangeType()));
		connect(multiplicityAction, SIGNAL(triggered()), this, SLOT(menuShowMultiplicityDialog()));
		connect(deleteAction, SIGNAL(triggered()), this, SLOT(menuDeleteItem()));

		/* Add the actions to the menu */
		menu->addAction(typeAction);
		menu->addAction(multiplicityAction);
		menu->addSeparator();
		menu->addAction(deleteAction);

		/* Open the context menu */
		menu->popup(contentView->viewport()->mapToGlobal(event->scenePos()).toPoint());
	}
	else {
		QGraphicsItem::mousePressEvent(event);
	}

}

void Arc::menuChangeType() {
	this->inhibitorArc = !inhibitorArc;
	if (inhibitorArc) {
		this->qGraphicsInhibitorItem->show();
		this->qGraphicsArrowItem->hide();
	}
	else {
		this->qGraphicsInhibitorItem->hide();
		this->qGraphicsArrowItem->show();
	}
}

void Arc::menuShowMultiplicityDialog()
{

	const auto qInputDialog = new QInputDialog(nullptr);
	qInputDialog->setInputMode(QInputDialog::IntInput);
	qInputDialog->setWindowTitle("Change Arc Multiplicity");
	qInputDialog->setLabelText("Multiplicity:");
	qInputDialog->setFont(*globalFont);
	qInputDialog->setIntMinimum(1);
	qInputDialog->setIntMaximum(100000);
	qInputDialog->setIntStep(1);
	qInputDialog->setIntValue(multiplicity);

	if (qInputDialog->exec()) {
		this->multiplicity = qInputDialog->intValue();
		showMultiplicity();
	}
}

void Arc::menuDeleteItem() const
{
	delete this;
}

void Arc::drawLine()
{
	// Set Arc in Main Components
	startPoint->addArcAsStartPoint(this);
	endPoint->addArcAsEndPoint(this);

	//LINE
	const auto line = QLineF(startPoint->getQGraphicsItem()->scenePos(),endPoint->getQGraphicsItem()->scenePos());

	this->qGraphicsPathItem = new ArcLinePath(this, line);
	//this->contentViewGraphicsScene->addItem(this->qGraphicsPathItem);

	// ARROW
	const QPolygonF arrow = getArrow(line);
	this->qGraphicsArrowItem = this->contentViewGraphicsScene->addPolygon(arrow);
	this->qGraphicsArrowItem->setPen(QPen(QBrush(Qt::GlobalColor::black), 3.0));
	this->qGraphicsArrowItem->setBrush(QBrush(Qt::green));

	// INHIBITOR
	constexpr qreal diameter = 10;
	this->qGraphicsInhibitorItem = this->contentViewGraphicsScene->addEllipse(QRectF(0.0, 0.0, diameter, diameter));
	this->qGraphicsInhibitorItem->setPen(QPen(QBrush(Qt::GlobalColor::black), 3.0));
	this->qGraphicsInhibitorItem->setBrush(QBrush(Qt::green));
	

	const auto center = getPositionForInhibitorCircle(line, diameter/2);
	this->qGraphicsInhibitorItem->setPos(center);

	this->addToGroup(this->qGraphicsPathItem);
	this->addToGroup(this->qGraphicsInhibitorItem);
	this->addToGroup(this->qGraphicsArrowItem);

	if (inhibitorArc)
		this->qGraphicsArrowItem->hide();
	else
		this->qGraphicsInhibitorItem->hide();

	updateView();
}

void Arc::fontChanged()
{
	if (multiplicityTextItem != nullptr)
	{
		showMultiplicity();
	}
}

bool Arc::couldFire(bool automaticRun) const
{
	// Arc from Transition to Place -> check if Place can handle the new tokens (not bigger than capacity)
	if (startPoint->getPetriNetMainComponentType() == PetriNetMainComponentType::Transition)
	{
		const auto end = dynamic_cast<Place *>(endPoint);
		return end->couldAddToken(multiplicity, automaticRun);
	}

	// Arc from Place to Transition
	const auto start = dynamic_cast<Place*>(startPoint);
	if (inhibitorArc)
	{
		return start->hasNoTokens();
	}

	return start->hasAtLeastNTokens(multiplicity, automaticRun);

}

void Arc::fire() const
{
	// Arc from Transition to Place -> add tokens to Place
	if (startPoint->getPetriNetMainComponentType() == PetriNetMainComponentType::Transition)
	{
		const auto end = dynamic_cast<Place*>(endPoint);
		return end->addTokens(multiplicity);
	}

	// Arc from Place to Transition
	if (!inhibitorArc)
	{
		// No Inhibitor --> reduce Tokens in Startplace 
		const auto start = dynamic_cast<Place*>(startPoint);
		start->reduceTokens(multiplicity);
	}

}

void Arc::reserveTokensForAutomaticRun() const
{
	// Arc from Transition to Place -> add tokens to Place
	if (startPoint->getPetriNetMainComponentType() == PetriNetMainComponentType::Transition)
	{
		const auto end = dynamic_cast<Place*>(endPoint);
		return end->addTokensWillBeAdded(multiplicity);
	}

	// Arc from Place to Transition
	if (!inhibitorArc)
	{
		// No Inhibitor --> reduce Tokens in Startplace 
		const auto start = dynamic_cast<Place*>(startPoint);
		start->addTokensWillBeRemoved(multiplicity);
	}


}

void Arc::drawLineWithKinks(const QList<QPointF>& kinks)
{
	drawLine();

	if (!kinks.empty())
	{
		qGraphicsPathItem->replaceKinks(kinks);
		updateView();
	}

	if (multiplicity != 1)
		showMultiplicity();
}

void Arc::moveItems(QLineF line)
{
	const QPolygonF arrow = getArrow(line);
	this->qGraphicsArrowItem->setPolygon(arrow);

	const auto pos = getPositionForInhibitorCircle(line, 5);
	this->qGraphicsInhibitorItem->setPos(pos);

	if (multiplicityTextItem != nullptr) {
		multiplicityTextItem->setPos(line.center());
	}
	
	this->removeFromGroup(this->qGraphicsPathItem);
	this->removeFromGroup(this->qGraphicsInhibitorItem);
	this->removeFromGroup(this->qGraphicsArrowItem);

	this->addToGroup(this->qGraphicsPathItem);
	this->addToGroup(this->qGraphicsInhibitorItem);
	this->addToGroup(this->qGraphicsArrowItem);
}

void Arc::showMultiplicity()
{
	const QString text = std::to_string(multiplicity).c_str();
	if (multiplicityTextItem == nullptr) {
		multiplicityTextItem = this->contentViewGraphicsScene->addText(text);
		multiplicityTextItem->setFont(*globalFont);

		multiplicityTextItem->setPos(this->qGraphicsPathItem->getLastLine().center());
	}
	else {
		multiplicityTextItem->setFont(*globalFont);
		multiplicityTextItem->setPlainText(text);
	}
}

void Arc::updateView()
{
	const QPointF start = startPoint->getQGraphicsItem()->pos() + startPoint->getQGraphicsItem()->boundingRect().center();
	const QPointF end = endPoint->getQGraphicsItem()->pos() + endPoint->getQGraphicsItem()->boundingRect().center();

	internalMoveStartPoint(start);
	internalMoveEndPoint(end);

	moveItems(this->qGraphicsPathItem->getLastLine());
}

void Arc::internalMoveStartPoint(QPointF newCenter) const
{
	const auto endOfFirstLine = this->qGraphicsPathItem->getEndOfFirstLine();

	// eine invertierte Linie wird genutzt damit die Linie auf die Componente zeigt
	QLineF invertedLine(endOfFirstLine, newCenter);
	const double offsetLength = calculateOffsetDependingOnType(startPoint, invertedLine);

	invertedLine.setLength(invertedLine.length() - offsetLength);

	// Der Endpunkt der Linie (Schnittpunkt Komponente und Line) wird zum Startpunkt des ArcLinePaths
	this->qGraphicsPathItem->setStartPoint(invertedLine.p2());
}


void Arc::internalMoveEndPoint(QPointF newCenter) const
{
	const auto beginOfLastLine = this->qGraphicsPathItem->getBeginOfLastLine();

	QLineF lastLine(beginOfLastLine, newCenter);
	const double offsetLength = calculateOffsetDependingOnType(endPoint, lastLine);
	
	lastLine.setLength(lastLine.length() - offsetLength);
	this->qGraphicsPathItem->setEndPoint(lastLine.p2());
}

double Arc::calculateOffsetDependingOnType(PetriNetMainComponent* component, QLineF lineShowToTheComponent) const
{
	switch (component->getPetriNetMainComponentType())
	{
	case PetriNetMainComponentType::Place:
		return component->getQGraphicsItem()->boundingRect().width() / 2;

	case PetriNetMainComponentType::Transition:

		//Linie schneidet die untere oder obere Kante?
		if ((lineShowToTheComponent.angle() > 45 && lineShowToTheComponent.angle() < 135) || 
			(lineShowToTheComponent.angle() > 225 && lineShowToTheComponent.angle() < 315))
		{
			return (component->getQGraphicsItem()->boundingRect().width() / 2) / std::abs(std::sin(lineShowToTheComponent.angle() * PI / 180));
		}
		
		//Linie schneidet die linke oder rechte Kante
		return (component->getQGraphicsItem()->boundingRect().width() / 2) / std::abs(std::cos(lineShowToTheComponent.angle() * PI / 180));

	}

	return 0;
}



QPolygonF Arc::getArrow(QLineF line) {
	QLineF angleline;

	/* Set the origin: */
	angleline.setP1(line.p2());

	/* Set the angle and length: */
	angleline.setAngle((line.angle() - 180) + 25);
	angleline.setLength(20);

	const auto p2 = angleline.p2();

	QLineF angleline2;

	/* Set the origin: */
	angleline2.setP1(line.p2());

	/* Set the angle and length: */
	angleline2.setAngle((line.angle() - 180) - 25);
	angleline2.setLength(20);

	const auto p3 = angleline2.p2();

	QPolygonF polygon;

	polygon.append(line.p2());
	polygon.append(p2);
	polygon.append(p3);

	return polygon;
}

QPointF Arc::getPositionForInhibitorCircle(QLineF line, double radius) {

	// Der Kreis soll nicht direkt am Endpunkt liegen, sondern ihn nur ber�hren
	auto center = line.pointAt(1 - (radius * 4 / 5) / line.length());

	// Da man f�r das Rectangle immer die Position oben links angeben muss muss jeweils der Radius abgezogen werden
	center.setX(center.x() - radius);
	center.setY(center.y() - radius);
	return center;
}
