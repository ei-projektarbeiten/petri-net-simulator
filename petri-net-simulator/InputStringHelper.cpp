#include "InputStringHelper.h"

#include <QRegularExpressionValidator>

QList<QString> InputStringHelper::extractInputNames(const QString& text)
{

    const QRegularExpression separators("[!&|]");

    QStringList list;

    // split the text
    long long mem = 0;
    long long i = text.indexOf(separators);
    while (i >= 0 && i < text.size()) {
        if (i != mem) list.append(text.mid(mem, i - mem)); // append str before separator

        if (text.mid(i, 1) == text.mid(i + 1ll, 1))  //  conjunction: OR || / AND &&
        {
            //list.append(text.mid(i, 2));               // append conjunction: && / ||
            i += 2;
        }
        else
        {
            //list.append(text.mid(i, 1));               // append negation: !
            i++;
        }

        mem = i;
        i = text.indexOf(separators, i);
    }

    if (mem < text.size())
        list.append(text.mid(mem, text.size() - mem));

    //auto list = text.split(QRegularExpression("([!&|])"));
    for (const auto& separatorNegationOrInputName : list)
    {
        qInfo(separatorNegationOrInputName.toStdString().c_str());
    }

    return list.toList();
}

QStringList InputStringHelper::splitInputConditionsIntoList(const QString& text)
{
	static QRegularExpression separators("[!&|]");

	QStringList list;

	long long mem = 0;
	long long position = text.indexOf(separators);
	while (position >= 0 && position < text.size()) {
		if (position != mem) list.append(text.mid(mem, position - mem)); // append str before separator

		if (text.mid(position, 1) == text.mid(position + 1ll, 1))  //  conjunction: OR || / AND &&
		{
			list.append(text.mid(position, 2));               // append conjunction: && / ||
			position += 2;
		}
		else
		{
			list.append(text.mid(position, 1));               // append negation: !
			position++;
		}

		mem = position;
		position = text.indexOf(separators, position);
	}

	if (mem < text.size())
		list.append(text.mid(mem, text.size() - mem));

    return list;
}

bool InputStringHelper::checkSyntax(const QString& text)
{

    // split the text
    QStringList splitConditions = splitInputConditionsIntoList(text);

    //auto list = text.split(QRegularExpression("([!&|])"));
    for (const auto& chars : splitConditions)
    {
        qInfo(chars.toStdString().c_str());
    }

    return checkForNegationOrText(splitConditions, 0);
}

/**
 * \brief check if inputConditions are fulfilled. Evaluation order ! --> && --> ||
 * \param inputCondition String entered by the user
 * \param inputValues boolean values for the user defined vars
 * \return true | false
 */
bool InputStringHelper::areInputConditionsFulfilled(const QString& inputCondition, const QMap<QString, bool>& inputValues)
{
    if (inputCondition.isNull())
        return true;

    auto inputConditions = splitInputConditionsIntoList(inputCondition);

    QList<QVariant> booleanExpressionList;
    for(const auto& listItem: inputConditions)
    {
	    if (listItem == "!" || listItem == "||" || listItem == "&&")
            booleanExpressionList.append(listItem);
        else
            booleanExpressionList.append(inputValues.value(listItem));
    }

    //for (auto exp : booleanExpressionList)
	//{
	//    qInfo() << exp.toString();
	//}

    // First remove !
    long long position = booleanExpressionList.indexOf("!", 0);
    while (position != -1)
    {
        booleanExpressionList.removeAt(position);
        const QVariant& oldValue = booleanExpressionList.at(position);
        booleanExpressionList.replace(position,!oldValue.toBool());

        position = booleanExpressionList.indexOf("!", position);
    }

    //for (auto exp : booleanExpressionList)
    //{
    //    qInfo() << exp.toString();
    //}

	// Then remove &&
    position = booleanExpressionList.indexOf("&&", 0);
    while (position != -1)
    {
        const bool oldValue1 = booleanExpressionList.at(position-1ll).toBool();
        const bool oldValue2 = booleanExpressionList.at(position+1ll).toBool();

    	booleanExpressionList.replace(position - 1ll, oldValue1 && oldValue2);

        booleanExpressionList.removeAt(position);
        booleanExpressionList.removeAt(position);

        position = booleanExpressionList.indexOf("&&", position);
    }

    //for(auto exp: booleanExpressionList)
    //{
    //    qInfo() << exp.toString();
    //}

    // Then remove ||
    position = booleanExpressionList.indexOf("||", 0);
    while (position != -1)
    {
        const bool oldValue1 = booleanExpressionList.at(position - 1ll).toBool();
        const bool oldValue2 = booleanExpressionList.at(position + 1ll).toBool();

        booleanExpressionList.replace(position - 1ll, oldValue1 || oldValue2);

        booleanExpressionList.removeAt(position);
        booleanExpressionList.removeAt(position);

        position = booleanExpressionList.indexOf("||", position);
    }

    if (booleanExpressionList.size() != 1)
    {
        qInfo("Something unexpected happened here");
    }

	return booleanExpressionList.at(0).toBool();
}

bool InputStringHelper::checkForNegationOrText(const QStringList& list, int i)
{
    static  QRegularExpression stringPattern{ "[A-Za-z0-9]" };
    //QRegularExpressionValidator stringValidator(stringPattern);
    if (list.at(i) == "!")
    {
        i++;
    }
    const auto isString = stringPattern.match(list.at(i));

    if (!isString.hasMatch())
    {
        return false;
    }

    i++;

    if (i == list.size())
        return true;

    return checkForConjunction(list, i);
}

bool InputStringHelper::checkForConjunction(const QStringList& list, int i)
{
    if (list.at(i) == "&&" || list.at(i) == "||")
    {
        i++;
        if (i == list.size())
            return false;                           // Expression darf nicht mit Conjunction enden

        return checkForNegationOrText(list, i);
    }
    return false;
}