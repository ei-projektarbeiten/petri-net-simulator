#include "Place.h"

#include "PetriNetMainComponent.h"
#include <QMenu>
#include <sstream>

#include "SetOutputsForPlace.h"

using namespace std;
using namespace std::chrono;

Place::~Place()
{
    this->contentViewGraphicsScene->removePlaceFromList(this);
    this->contentViewGraphicsScene = nullptr;
}

Place::Place(ContentViewGraphicsScene* contentViewGraphicsScene, unsigned id, QFont* newGlobalFont) :
    QGraphicsEllipseItem(QRectF(0.0,0.0,30,30)), 
    PetriNetMainComponent(id, PetriNetMainComponentType::Place, newGlobalFont)
{
    this->contentViewGraphicsScene = contentViewGraphicsScene;

    this->setFlag(QGraphicsItem::ItemIsSelectable, true);
    this->setFlag(QGraphicsItem::ItemIsMovable, true);
    this->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);

    itemPen = new QPen(Qt::black);
    itemPen->setWidth(3);
    this->setPen(*itemPen);

    // Bring to foreground
    this->setZValue(1);

    tokens = 0;
    tokensGraphicsItem = nullptr;

    // -1 = no limit
    capacity = -1;
    textItemUnderPlace = nullptr;
    countTokensWillBeAdded = 0;
    countTokensWillBeRemoved = 0;
}

Place::Place(ContentViewGraphicsScene* contentViewGraphics, QFont* font, int id, int tokens, int capacity,
	const QString& newName, const QList<Output>& list):Place(contentViewGraphics, id, font)
{
    this->capacity = capacity;
    this->name = newName;
    this->outputs = list;
    updateTokens(tokens);

    updateTextView();
}

void Place::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    qInfo("Place Clicked ");

    if (event->button() == Qt::LeftButton 
        && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::Arc)
    {
        contentViewGraphicsScene->addArcToComponent(this);
    }
    else if (event->button() == Qt::LeftButton
        && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::AddToken)
    {
        updateTokens(tokens + 1);
    }

    else if (event->button() == Qt::RightButton
        && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::AddToken)
    {
        if (tokens > 0)
        {
            updateTokens(tokens - 1);
        }
    }
    else if (event->button() == Qt::RightButton
        && contentViewGraphicsScene->getContentView()->getContentViewCursor() == ContentViewCursor::Select
        && this->contains(event->pos()))
    {
        // open Menu
        openContextMenu(event->scenePos());
    }
    else
    {
        QGraphicsEllipseItem::mousePressEvent(event);
    }
}

void Place::openContextMenu(QPointF scenePos)
{
	const auto contentView = this->contentViewGraphicsScene->getContentView();

    /* Create an object context menu */
	const auto menu (new QMenu(contentView));
    menu->setFont(*globalFont);

    /* Create actions to the context menu */
	const auto setCapacity (new QAction("set capacity", this));
    const auto setTokens (new QAction("set tokens", this));
	const auto setLabel (new QAction("set name", this));
    const auto setOutputs (new QAction("set outputs", this));

	const auto deleteItem (new QAction("delete", this));
    deleteItem->setShortcut(Qt::Key_Delete);

    // disable Actions but not setTokens
    if (this->contentViewGraphicsScene->getContentView()->getAppMode() != AppMode::Edit)
    {
        setCapacity->setEnabled(false);
        
        setLabel->setEnabled(false);
        setOutputs->setEnabled(false);
        deleteItem->setEnabled(false);
    }

    /* Connect QAction Signals to Arc Slots */
    connect(setCapacity, SIGNAL(triggered()), this, SLOT(menuSetCapacity()));
	connect(setTokens, SIGNAL(triggered()), this, SLOT(menuSetTokens()));
    connect(setLabel, SIGNAL(triggered()), this, SLOT(menuSetName()));
    connect(setOutputs, SIGNAL(triggered()), this, SLOT(menuSetOutputs()));
    connect(deleteItem, SIGNAL(triggered()), this, SLOT(menuDeleteItem()));

    /* Set the actions to the menu */
    menu->addAction(setCapacity);
    menu->addAction(setTokens);
	menu->addAction(setLabel);
    menu->addSeparator();
    menu->addAction(setOutputs);
    menu->addSeparator();
    menu->addAction(deleteItem);

    /* Open the context menu */
    menu->popup(contentView->viewport()->mapToGlobal(scenePos).toPoint());
}

void Place::menuSetName()
{
    qInfo("Context menu Edit Label Clicked");

    QString text = "";
    if (!name.isNull())
    {
        text = name;
    }

    const auto qInputDialog = new QInputDialog(nullptr);
    qInputDialog->setInputMode(QInputDialog::TextInput);
    qInputDialog->setWindowTitle("Name of Place");
    qInputDialog->setLabelText("Name: ");
    qInputDialog->setFont(*globalFont);
    qInputDialog->setTextValue(text);
    qInputDialog->setTextEchoMode(QLineEdit::Normal);

    if (qInputDialog->exec())
    {
        // Operation was successful

        if (qInputDialog->textValue().isEmpty())
        {
            name = nullptr;
        }
        else {
            name = qInputDialog->textValue();
        }

        updateTextView();
    }
}

void Place::fireAutomaticUpdateTokenCount()
{
    tokens = countTokensWillBeAdded - countTokensWillBeRemoved + tokens;
    countTokensWillBeAdded = 0;
    countTokensWillBeRemoved = 0;
    updateTokensInView();
}

void Place::menuDeleteItem()
{
    qInfo("Context menu delete Clicked");

    deleteAllConnectedArcs();
    delete this;
}

void Place::menuSetOutputs()
{
    qInfo("Context menu set Outputs Clicked");
    SetOutputsForPlace qInputDialog(outputs, name, globalFont);

    if (qInputDialog.exec() != QDialog::Accepted)
    {
        // Operation was aborted
        return;
    }


    this->outputs = qInputDialog.getResult();
    qInfo("Output Count: + %lld", outputs.count());

    updateTextView();
}


void Place::menuSetCapacity()
{

    const auto qInputDialog = new QInputDialog(nullptr);
    qInputDialog->setInputMode(QInputDialog::IntInput);
    qInputDialog->setWindowTitle("Set Capacity of Place");
	qInputDialog->setLabelText("Capacity (-1: No Limit)");
    qInputDialog->setFont(*globalFont);
    qInputDialog->setIntMinimum(-1);
    qInputDialog->setIntMaximum(1000000);
    qInputDialog->setIntStep(1);
    qInputDialog->setIntValue(capacity);

    if (qInputDialog->exec()) {
        capacity = qInputDialog->intValue();
        updateTextView();
        if (capacity != -1 && capacity < tokens)
        {
            tokens = capacity;
            updateTokensInView();
        }
    }
}

void Place::menuSetTokens()
{
    const auto qInputDialog = new QInputDialog(nullptr);
    qInputDialog->setInputMode(QInputDialog::IntInput);
    qInputDialog->setWindowTitle("Set Tokens of Place");
    qInputDialog->setLabelText("Tokens (0 ... n)");
    qInputDialog->setFont(*globalFont);
    qInputDialog->setIntMinimum(0);
    qInputDialog->setIntMaximum(1000000);
    qInputDialog->setIntStep(1);
    qInputDialog->setIntValue(tokens);

    if (qInputDialog->exec()) {
        updateTokens(qInputDialog->intValue());
        
    }
}

void Place::updateTokens(int newTokenValue)
{
    if (capacity != -1 && capacity < newTokenValue)
    {
        tokens = capacity;
    }
    else {
        tokens = newTokenValue;
    }
    updateTokensInView();
    this->contentViewGraphicsScene->updateTransitionColor();
}

void Place::updateTokensInView()
{
    delete tokensGraphicsItem;
    tokensGraphicsItem = nullptr;

    if (tokens != 0)
    {
        tokensGraphicsItem = new QGraphicsTextItem(QString::number(tokens), this);
        tokensGraphicsItem->setFont(*globalFont);

        tokensGraphicsItem->setPos((this->rect().width() - tokensGraphicsItem->boundingRect().width()) / 2, 0);
    }
    
}

void Place::updateTextView()
{
    std::stringstream ss;
    
    if (!name.isNull() && !name.isEmpty())
    {
        ss << name.toStdString() << "<br>";
    }

    if (capacity != -1)
    {
        ss << "Capacity: " << std::to_string(capacity) << "<br>";
    }

    for (int i = 0; i < outputs.count(); i++)
    {
        Output output = outputs.at(i);
        ss << output.getName().toStdString() << " " << output.getRelationalOperator().toStdString() << " " << std::to_string(output.getValue()) << "<br>";
        //ss << output.getName().toStdString() << " = tokens " << output.getRelationalOperator().toStdString() << std::to_string(output.getValue()) << "<br>";
    }

	delete textItemUnderPlace;
    
    textItemUnderPlace = new QGraphicsTextItem(this);
    textItemUnderPlace->setFont(*globalFont);
    textItemUnderPlace->setHtml(QString::fromStdString("<div style='background:rgba(255, 255, 255, 60%);'>" + ss.str().substr(0, ss.str().length()-4) + "</div>"));

	textItemUnderPlace->setPos((this->rect().width() - textItemUnderPlace->boundingRect().width()) / 2, this->rect().height());

    textItemUnderPlace->adjustSize();

    // Centers the Text
    QTextBlockFormat format;
    format.setAlignment(Qt::AlignCenter);
    QTextCursor cursor = textItemUnderPlace->textCursor();
    cursor.select(QTextCursor::Document);
    cursor.mergeBlockFormat(format);
    cursor.clearSelection();
    textItemUnderPlace->setTextCursor(cursor);
}

QVariant Place::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if (change == ItemPositionChange && scene()) {
        moveLines();
    }
    else if (change == ItemSelectedChange) {
	    if (handleItemSelectedEvent(this, value))
        {
            return false;
        }
    }
    return QGraphicsItem::itemChange(change, value);
}

void Place::fontChanged()
{
    updateTextView();
    if (tokens > 0)
    {
        updateTokensInView();
    }
    
}

bool Place::couldAddToken(int tokensToAdd, bool automaticRun) const
{
    // unlimited Capacity
    if (capacity == -1)
    {
        return true;
    }

    if (automaticRun)
		return capacity >= tokens + tokensToAdd + countTokensWillBeAdded - countTokensWillBeRemoved;

    return capacity >= tokens + tokensToAdd;
}

bool Place::hasNoTokens() const
{
    return tokens == 0;
}

void Place::addTokensWillBeAdded(unsigned tokensToAdd)
{
    countTokensWillBeAdded += tokensToAdd;
}

void Place::addTokensWillBeRemoved(unsigned tokensToRemove)
{
    countTokensWillBeRemoved += tokensToRemove;
}

bool Place::hasAtLeastNTokens(unsigned n, bool automaticRun) const
{
    if (automaticRun)
        return tokens >= n + countTokensWillBeRemoved;
    return tokens >= n;
}

void Place::addTokens(unsigned tokensToAdd)
{
    tokens += tokensToAdd;
    updateTokensInView();
}

void Place::reduceTokens(unsigned tokensToSubstract)
{
    tokens -= tokensToSubstract;
    updateTokensInView();
}

void Place::unselectItem()
{
    this->setSelected(false);
}
