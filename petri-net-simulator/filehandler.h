#pragma once
#include "InputStringHelper.h"

class FileHandler
{
public:
	static QString contentViewToJson(const ContentView*);
    static ContentView* jsonToContentViewPtr(MainWindow* mainWindow, QFont* font, const QByteArray& json);

private:
	FileHandler() = default;
};
