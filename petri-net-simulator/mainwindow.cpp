#include "MainWindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"
#include "QJsonParseError"
#include <QDebug>
#include <QShortcut>
#include <QFontDialog>
#include <QSettings>
#include <QMessageBox>

#include "InOutputWindow.h"
#include "ContentView.h"
#include "FileHandler.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , inOutputWindow(nullptr)
{
    ui->setupUi(this);

    this->setAttribute(Qt::WA_DeleteOnClose);

    mySettings = new QSettings(QSettings::UserScope, "THM", "Petri Net Simulator", this);

    loadGlobalFont();

    fontChanged();

    contentView = new ContentView(this, ContentViewCursor::Select, AppMode::Edit, globalFont);

    this->setCentralWidget(contentView);

    deleteItem = new QShortcut(QKeySequence(Qt::Key_Delete), this, SLOT(deleteSelectedItem()));

    this->setAppMode(AppMode::Edit);
}

MainWindow::~MainWindow()
{
    delete inOutputWindow;  
    // delete mySettings;       // Destroyed by QT
    // delete deleteItem;       // Destroyed by QT
    // delete contentView;      // Destroyed by QT
    delete ui;
}

void MainWindow::inOutputWindowClosed()
{
    this->inOutputWindow = nullptr;
    on_actionModeEdit_triggered();
}

int MainWindow::askUserForPermission(QString& text)
{
	QMessageBox msgBox;
    msgBox.setWindowTitle("Petri Net Simulator - Ask for confirmation");
	msgBox.setText(text);
	msgBox.setInformativeText("All unsaved changes will be lost!");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.setIcon(QMessageBox::Question);

	return msgBox.exec();
}

void MainWindow::on_actionFileNew_triggered()
{
    QString text = "Open a new file?";
    if (askUserForPermission(text) == QMessageBox::Ok)
    {
        delete contentView;
        contentView = new ContentView(this, ContentViewCursor::Select, AppMode::Edit, globalFont);

        this->setCentralWidget(contentView);
        filePath = nullptr;

        setAppMode(AppMode::Edit);
    }

}

void MainWindow::on_actionFileLoad_triggered()
{
    QFileDialog dialog(this);

    QStringList filters;

    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setWindowTitle("select a file to open");
    dialog.setFileMode(QFileDialog::ExistingFile);
    filters.append("application/json");
    dialog.setMimeTypeFilters(filters);
    dialog.setNameFilter("*.json");

    const int result = dialog.exec();

    qInfo("%i", result);

    const QStringList list = dialog.selectedFiles();

    if (list.count() == 1)
    {
	    const QString tmpFilePath = list.at(0);

        qInfo(tmpFilePath.toStdString().c_str());

        QFile file(tmpFilePath);
        if (file.exists() && file.open(QFile::ReadWrite)) {

            const QByteArray data = file.readAll();
        	file.close();

            const auto newContentView = FileHandler::jsonToContentViewPtr(this, globalFont, data);
            if (newContentView != nullptr)
            {
                QString text = "Load file?";
                if (askUserForPermission(text) == QMessageBox::Ok)
                {
                    delete this->contentView;
                    this->contentView = newContentView;
                    this->setCentralWidget(newContentView);
                    this->filePath = tmpFilePath;
                    setAppMode(AppMode::Edit);
                }
            	else
                {
                    delete newContentView;
                }
            }

        } else {
            qInfo("File does not exist anymore");
        }
    }

}

void MainWindow::on_actionFileSave_triggered()
{
    qInfo("Save");

    if (filePath.isNull())
    {
        on_actionFileSaveAs_triggered();
    }
    else
    {
        QFile file(filePath);
        if (file.open(QFile::WriteOnly)) {
	        const auto json = FileHandler::contentViewToJson(contentView);
            file.write(json.toUtf8());
            file.close();
        }
    }
    
}

void MainWindow::on_actionFileSaveAs_triggered()
{
    qInfo( "SaveAs");

    QFileDialog dialog(this);

    QStringList filters;

    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setWindowTitle("Save File to");
    dialog.setFileMode(QFileDialog::AnyFile);
    filters.append("application/json");
    dialog.setMimeTypeFilters(filters);
    //dialog.setNameFilter("*.json");

    const int result = dialog.exec();


    qInfo("%i", result);

    const QStringList list = dialog.selectedFiles();

    if (list.count() == 1)
    {
        filePath = list.at(0);

        qInfo(filePath.toStdString().c_str());

        QFile file(filePath);
        if (file.open(QFile::WriteOnly)) {
	        const auto json = FileHandler::contentViewToJson(contentView);
            file.write(json.toUtf8());
            file.close();
        }
    }

    updateTitle();
}

void MainWindow::on_actionSettingsChangeFontSize_triggered()
{
    bool ok;
    qInfo("CHANGE FONT SIZE");
    const QFont font = QFontDialog::getFont(
        &ok, QFont(globalFont->family(), globalFont->pointSize()), this);

    if (ok) {
        globalFont->setFamily(font.family());
    	globalFont->setPointSize(font.pointSize());

        mySettings->setValue("FontPointSize", font.pointSize());
        mySettings->setValue("FontFamily", font.family());

        this->fontChanged();
        contentView->fontChanged();
    }
}


void MainWindow::on_actionSettingsZoomIn_triggered() const
{
    contentView->scale(1.2, 1.2);
}

void MainWindow::on_actionSettingsZoomOut_triggered() const
{
    contentView->scale(0.8, 0.8);
}

void MainWindow::on_actionModeEdit_triggered()
{
    this->setAppMode(AppMode::Edit);
}

void MainWindow::on_actionModeManual_triggered()
{
    this->setAppMode(AppMode::Manual);
}

void MainWindow::on_actionModeAutomatic_triggered()
{
    delete inOutputWindow;
    inOutputWindow = nullptr;

    this->setAppMode(AppMode::Automatic);

    const QList<QString> outputs = contentView->getOutputNames();
    const QList<QString> inputs = contentView->getInputNames();
    inOutputWindow = new InOutputWindow(globalFont, inputs, outputs, this); 

    inOutputWindow->show();

    connect(inOutputWindow, SIGNAL(close()), this, SLOT(on_actionModeEdit_triggered()));

    connect(inOutputWindow, SIGNAL(closeEvent()), this, SLOT(on_actionModeEdit_triggered()));

}

void MainWindow::on_actionSelect_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::Select);
}

void MainWindow::on_actionEditPlace_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::Place);
}

void MainWindow::on_actionEditTransition_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::Transition);
}

void MainWindow::on_actionEditArc_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::Arc);
}

void MainWindow::on_actionRunAddMarker_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::AddToken);
}


void MainWindow::on_actionRunFireTransition_triggered() const
{
    this->contentView->setCurrentCursor(ContentViewCursor::FireTransition);
}


void MainWindow::on_actionRunAutoRun_triggered() const
{
    if (this->inOutputWindow == nullptr)
    {
        return;
    }

    const auto inputMap = this->inOutputWindow->getInputValues();

    const auto outputs = this->contentView->automaticRunTriggered(inputMap);

    this->inOutputWindow->updateOutputValues(outputs);

    this->inOutputWindow->raise();
}

void MainWindow::on_actionRunExit_triggered()
{
    this->setAppMode(AppMode::Edit);
}

void MainWindow::deleteSelectedItem() const
{
    contentView->deleteSelectedItem();
}

void MainWindow::loadGlobalFont()
{
    globalFont = new QFont();
    globalFont->setPointSize(mySettings->value("FontPointSize", 10).toInt());
    globalFont->setFamily(mySettings->value("FontFamily", "Segoe UI").toString());
}

void MainWindow::setAppMode(AppMode state)
{

    if (this->contentView->getAppMode() == AppMode::Automatic)
    {
	    delete this->inOutputWindow;
        this->inOutputWindow = nullptr;
    }

    this->contentView->setAppState(state);

    ui->actionEditArc->setEnabled(false);
    ui->actionEditPlace->setEnabled(false);
    ui->actionEditTransition->setEnabled(false);
    ui->actionRunFireTransition->setEnabled(false);
    ui->actionRunAddMarker->setEnabled(false);
    ui->actionRunAutoRun->setEnabled(false);
    ui->actionRunExit->setEnabled(false);
    switch (state) {
    case AppMode::Edit:
        ui->actionEditArc->setEnabled(true);
        ui->actionEditPlace->setEnabled(true);
        ui->actionEditTransition->setEnabled(true);
        break;

    case AppMode::Manual:
        ui->actionRunFireTransition->setEnabled(true);
        ui->actionRunAddMarker->setEnabled(true);
        ui->actionRunExit->setEnabled(true);
        break;

    case AppMode::Automatic:
        ui->actionRunAddMarker->setEnabled(true);
        ui->actionRunAutoRun->setEnabled(true);
        ui->actionRunExit->setEnabled(true);
        break;

    }

    this->updateTitle();

    this->contentView->setCurrentCursor(ContentViewCursor::Select);
}


void MainWindow::fontChanged() const
{
    this->ui->menubar->setFont(*globalFont);
    this->ui->menuFile->setFont(*globalFont);
    this->ui->menuSettings->setFont(*globalFont);
    this->ui->menuMode->setFont(*globalFont);
    this->ui->menuEdit->setFont(*globalFont);
    this->ui->menuSimulate->setFont(*globalFont);
}

void MainWindow::updateTitle()
{
    QString newTitle;
    QTextStream titleStream(&newTitle);
    titleStream << "Petri Net Simulator - ";
    if (filePath.isNull())
        titleStream << "unnamed";
    else
    {
        titleStream << filePath.mid(filePath.lastIndexOf("/") + 1);
    }
        
    titleStream << " - ";
    switch (this->contentView->getAppMode())
    {
    case AppMode::Edit:
        titleStream << "Edit";
        break;
    case AppMode::Automatic:
        titleStream << "Automatic";
        break;
    case AppMode::Manual:
        titleStream << "Manual";
        break;
    }

    setWindowTitle(newTitle);

}