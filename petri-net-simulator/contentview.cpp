#include "ContentView.h"
#include <QGraphicsScene>
#include <QSvgRenderer>
#include "ContentViewGraphicsScene.h"

ContentView::~ContentView()
{
    //delete graphicsScene;   // deleted by QT
}

void ContentView::fontChanged()
{
    this->setFont(*globalFont);
    this->graphicsScene->fontChanged();
}

QList<QString> ContentView::getOutputNames() const
{
    return graphicsScene->getOutputNames();
}

QList<QString> ContentView::getInputNames() const
{
    return graphicsScene->getInputNames();
}

QMap<QString, bool> ContentView::automaticRunTriggered(const QMap<QString, bool>& inputValues) const
{
    this->graphicsScene->fireTransitionAutomatically(inputValues);

    return this->graphicsScene->evaluateOutputs();
}

ContentView::ContentView(QWidget *parent, ContentViewCursor cursor, AppMode state, QFont* globalFont, unsigned nextId)
: QGraphicsView(parent)
{
    this->contentViewCursor = cursor;
    this->appMode = state;
    this->globalFont = globalFont;

    this->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    this->setFont(*globalFont);

    graphicsScene = new ContentViewGraphicsScene(this, globalFont, nextId);

    this->setBackgroundBrush(QBrush(Qt::yellow, Qt::SolidPattern));
    this->setScene(graphicsScene);

    this->setViewportUpdateMode(ViewportUpdateMode::FullViewportUpdate);

    graphicsScene->setSceneRect(0, 0, this->width() + 100, this->height() + 100);

    // Verbessert das Aussehen der Kreise, weniger eckig
    this->setRenderHint(QPainter::Antialiasing, true);
    this->setRenderHint(QPainter::TextAntialiasing, true);

}

void ContentView::deleteSelectedItem() const
{
    this->graphicsScene->deleteSelectedItem();
}

void ContentView::edit() const
{
    qInfo("Edit");

    const auto a = this->graphicsScene->selectedItems();

    qInfo("%lld", a.size());
}

void ContentView::slotCustomMenuRequested(QPoint point)
{
    qInfo("Custom Menu Requested, x: %d ; y: %d", point.x(), point.y());
}

void ContentView::setCurrentCursor(ContentViewCursor newCursor)
{
    this->contentViewCursor = newCursor;

	this->graphicsScene->clearSelection();
    this->graphicsScene->clearFocus();
    this->graphicsScene->cursorWillBeChanged();

    const QCursor* c = nullptr;

    switch (newCursor)
    {
    case ContentViewCursor::Place:
    {
        const QPixmap p = QIcon(":/images/place.svg").pixmap(QSize(30, 30));
        c = new QCursor(p, 0, 0);
        break;
    }

    case ContentViewCursor::Transition:
    {
	    const QPixmap p = QIcon(":/images/transition.svg").pixmap(QSize(20, 20));
        c = new QCursor(p, 0, 0);
        break;
    }

    case ContentViewCursor::Arc:
    {
	    const QPixmap p = QIcon(":/images/arrow-left-up-line.png").pixmap(QSize(20, 20));
        c = new QCursor(p, 0, 0);
        break;
    }

    case ContentViewCursor::AddToken:
    {
        const QPixmap p = QIcon(":/images/add-fill.png").pixmap(QSize(30, 30));
        c = new QCursor(p, 15, 15);
    	break;
    }

    case ContentViewCursor::FireTransition:
    {
        const QPixmap p = QIcon(":/images/fire-fill.png").pixmap(QSize(24, 24));
        c = new QCursor(p, 12, 12);
        break;
    }

    case ContentViewCursor::Select:
    {
        c = new QCursor(Qt::CursorShape::ArrowCursor);
        break;
    }

    }

    if (c == nullptr)
        c = new QCursor(Qt::CursorShape::ArrowCursor);
    
    this->setCursor(*c);
}

void ContentView::resizeEvent(QResizeEvent* event)
{
    QGraphicsView::resizeEvent(event);

    graphicsScene->windowWasResized(event->size());
    qInfo("event->size(): %d / %d / %d / %d", event->size().width(), event->size().height(), event->oldSize().width(), event->oldSize().height());
}

void ContentView::setAppState(AppMode newState) {
    this->appMode = newState;

    this->graphicsScene->updateMoveableStatus();
    this->graphicsScene->updateTransitionColor();
}

ContentViewCursor ContentView::getContentViewCursor() const
{
    return contentViewCursor;
}

AppMode ContentView::getAppMode() const
{
    return this->appMode;
}