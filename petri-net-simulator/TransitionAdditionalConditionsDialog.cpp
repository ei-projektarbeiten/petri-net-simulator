#include "TransitionAdditionalConditionsDialog.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>

#include "InputStringHelper.h"

TransitionAdditionalConditionsDialog::TransitionAdditionalConditionsDialog(QWidget* parent, const QString& text, const QFont* font) :
	QDialog(parent)
{
    setWindowTitle("Set additional transition conditions");

    le = nullptr;

    this->setAttribute(Qt::WA_QuitOnClose, false);

    const auto vbox = new QVBoxLayout;

    const auto label = new QLabel(tr("Conditions:"));
    label->setFont(*font);

    vbox->addWidget(label);

    le = new QLineEdit();
    le->setFont(*font);
    le->setText(text);
    le->setValidator(new QRegularExpressionValidator(QRegularExpression("([A-Za-z0-9&|!]){0,1000}")));

    vbox->addWidget(le);

    const auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    buttonBox->setFont(*font);

    vbox->addWidget(buttonBox);

    this->setLayout(vbox);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &TransitionAdditionalConditionsDialog::okClicked);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

QString TransitionAdditionalConditionsDialog::textValue() const
{
    return le->text();
}

void TransitionAdditionalConditionsDialog::okClicked()
{
	const QString text = le->text();

    if (text.size()== 0)
    {
        this->close();
        this->setResult(Accepted);
        return;
    }

    /*int openingTags = text.count("(");
    int closingTags = text.count(")");

    if (openingTags != closingTags)
    {
        qFatal("missing brackets (opening != closing brackets)");
    }*/

	const long long singleAnd = text.count("&");
	const long long doubleAnd = text.count("&&");

    if (singleAnd != doubleAnd * 2)
    {
        qFatal("somewhere is a single &");
    }

	const long long singleOr = text.count("|");
	const long long doubleOr = text.count("||");

    if (singleOr != doubleOr * 2)
    {
        qFatal("somewhere is a single |");
    }

    if (InputStringHelper::checkSyntax(text))
    {
        this->close();
        this->setResult(Accepted);
        return;
    }

    qInfo("Text was not correct");
}


