#pragma once
#include <qboxlayout.h>
#include <qdialog.h>

#include "Place.h"
#include <QTableWidget>

class SetOutputsForPlace :
    public QDialog
{

public:
	SetOutputsForPlace(QList<Output>&, const QString&, QFont*);
	~SetOutputsForPlace() override;
	QList<Output> getResult();

public slots:

    void okClicked();
	void addClicked();
	void trashClicked() const;

private:
	QHBoxLayout* getLayoutWithAddAndTrashButtons();
	void loadOutputsIntoTable(QList<Output>& outputs);

	QList<Output> userDefinedOutputs;
	QTableWidget* tableWidget;
	QFont* font;
};

