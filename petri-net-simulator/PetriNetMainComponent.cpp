#include "PetriNetMainComponent.h"

using namespace std::chrono;

PetriNetMainComponent::~PetriNetMainComponent()
{
	delete itemPen;

	// For security purpose:
	deleteAllConnectedArcs();
}

PetriNetMainComponentType PetriNetMainComponent::getPetriNetMainComponentType() const
{
	return this->type;
}

void PetriNetMainComponent::moveLines() const
{
	for (auto const & arc : startPointOfArcs) {
		arc->updateView();
	}

	for (auto const & arc : endPointOfArcs) {
		arc->updateView();
	}
}

bool PetriNetMainComponent::handleItemSelectedEvent(QAbstractGraphicsShapeItem* qGraphicsItem, const QVariant& value)
{
	if (value == true)
	{
		// Item is selected

		//item was added in the last 50ms 
		long long const now = duration_cast<milliseconds>(
			system_clock::now().time_since_epoch()
			).count();

		long long const diff = now - this->lastAddedArcUnixMs;
		qInfo("Diff %lld", diff);
		if (now - this->lastAddedArcUnixMs < 100)
		{
			this->lastAddedArcUnixMs = 0;
			qGraphicsItem->setSelected(false);
			return true;
		}

		itemPen->setColor(QColor(0, 191, 255));
		qGraphicsItem->setPen(*itemPen);
	}
	else {
		// Item is deselected

		itemPen->setColor(Qt::black);
		qGraphicsItem->setPen(*itemPen);
	}
	return false;
}

void PetriNetMainComponent::addArcAsStartPoint(Arc* newStartArc) {
	startPointOfArcs.push_back(newStartArc);

	// Save time to unset focus in handleItemSelectedEvent
	this->lastAddedArcUnixMs = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
		).count();
}

void PetriNetMainComponent::addArcAsEndPoint(Arc* newEndArc) {
	endPointOfArcs.push_back(newEndArc);

	// Save time to unset focus in handleItemSelectedEvent
	this->lastAddedArcUnixMs = duration_cast<milliseconds>(
			system_clock::now().time_since_epoch()
		).count();
}

void PetriNetMainComponent::removeStartPointArc(Arc* deletableArc)
{
	startPointOfArcs.remove(deletableArc);
}

void PetriNetMainComponent::removeEndPointArc(Arc* deletableArc)
{
	endPointOfArcs.remove(deletableArc);
}

void PetriNetMainComponent::deleteAllConnectedArcs()
{

	while (!startPointOfArcs.empty()) {
		const Arc* start = startPointOfArcs.front();
		startPointOfArcs.pop_front();
		delete start;
	}

	while (!endPointOfArcs.empty()) {
		const Arc* end = endPointOfArcs.front();
		endPointOfArcs.pop_front();
		delete end;
	}
}
