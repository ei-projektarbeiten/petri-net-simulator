#pragma once

#include <QGraphicsEllipseItem>

#include "ContentViewGraphicsScene.fwd.h"
#include "Output.h"
#include "PetriNetMainComponent.h"

class Place : QObject, public QGraphicsEllipseItem, public PetriNetMainComponent
{
    Q_OBJECT

public:
    ~Place() override;
    Place(ContentViewGraphicsScene* contentView, unsigned id, QFont* newGlobalFont);
    Place(ContentViewGraphicsScene* contentViewGraphics, QFont* font, int id, int tokens, int capacity, const QString& newName, const QList<Output>& list);
    void fontChanged();
    [[nodiscard]] bool couldAddToken(int tokensToAdd, bool automaticRun) const;
    [[nodiscard]] bool hasNoTokens() const;
    [[nodiscard]] bool hasAtLeastNTokens(unsigned, bool) const;
    void addTokens(unsigned int tokensToAdd);
    void reduceTokens(unsigned int tokensToSubstract);
    void addTokensWillBeAdded(unsigned int);
    void addTokensWillBeRemoved(unsigned int);
    void fireAutomaticUpdateTokenCount();
    [[nodiscard]] unsigned getTokenCount() const { return tokens; }
    [[nodiscard]] int getCapacity() const { return capacity; }
    [[nodiscard]] QGraphicsItem* getQGraphicsItem() override { return this; }

public slots:
    void menuSetName();
    void menuDeleteItem();
    void menuSetCapacity();
    void updateTokens(int newTokenValue);
    void menuSetTokens();
    void menuSetOutputs();
    const QList<Output>& getOutputs() { return outputs; }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;
    void unselectItem() override;

private:
    void openContextMenu(QPointF pos);
    void updateTextView();
    void updateTokensInView();

    ContentViewGraphicsScene* contentViewGraphicsScene;

    unsigned tokens;
    QGraphicsTextItem* tokensGraphicsItem;

    int capacity;
    QGraphicsTextItem* textItemUnderPlace;
    QList<Output> outputs;
    unsigned countTokensWillBeAdded;
    unsigned countTokensWillBeRemoved;
};
