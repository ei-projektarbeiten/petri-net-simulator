#pragma once
#include <QList>
#include <qstring.h>

//enum class ERelationalOperator {
//	eq, ne, gt, ge, lt, le
//};

class Output
{

public:
	Output(const QString& name, const QString& relationalOperator, const unsigned int value) { this->name = name; this->relationalOperator = relationalOperator; this->value = value; }
	[[nodiscard]] QString getName() { return name; }
	[[nodiscard]] QString getRelationalOperator() { return relationalOperator; }
	[[nodiscard]] static QStringList getAllRelationalOperators() { return allRelationalOperators; }
	[[nodiscard]] unsigned int getValue() const { return value; }
	[[nodiscard]] long long getRelationalOperatorIndex() const;

private:
	QString name;
	//ERelationalOperator relationalOperator;
	QString relationalOperator;
	unsigned int value;
	inline static const QStringList allRelationalOperators = { "==", "!=", ">", ">=", "<", "<=" };

};



